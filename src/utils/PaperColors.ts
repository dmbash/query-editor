import Paper from 'paper';

const PaperColors = {
	Black: new Paper.Color('#1A1A18'),
	Gray: new Paper.Color('#E8E0E0'),
	Yellow: new Paper.Color('#F9C34E'),
	Red: new Paper.Color('#DE2B2B'),
	DeepBlue: new Paper.Color('#285ED2'),
	LightBlue: new Paper.Color('#4E84F9'),
	White: new Paper.Color('#FCFCFC')
};

export default PaperColors;
