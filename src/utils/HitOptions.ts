const HitOptions = (tolerance: number, fill: boolean = true, stroke: boolean = true, segments: boolean = true, handles = true) => ({
	fill,
	stroke,
	segments,
	handles,
	tolerance
});

export default HitOptions;
