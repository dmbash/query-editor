const IsRightClicked = (event : any) : boolean => {
	if (Object.prototype.hasOwnProperty.call(event, 'event')) {
		const nestedEvent = event.event;
		return nestedEvent.button === 2;
	}
	return false;
}

export default IsRightClicked;
