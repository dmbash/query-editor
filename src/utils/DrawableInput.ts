import Paper from 'paper';
import CanvasTextInput from '../constraints/CanvasInputText';

const makeInput = (value: string, initialLimit: string, placementPoint: paper.Point, color: paper.Color, onNewValueCb?: Function) => {
	const groupText = new Paper.Group();
	const text = new Paper.PointText(placementPoint);

	text.content = value.toString() + ' ' + '±';
	text.fillColor = color;
	text.fontSize = 18;
	text.justification = 'center';

	const pointAfterText = text.point.clone();
	pointAfterText.x = pointAfterText.x + text.bounds.width;

	const editableText = new CanvasTextInput(pointAfterText, onNewValueCb);
	editableText.content = initialLimit;
	editableText.fillColor = color;
	editableText.fontSize = 18;
	editableText.justification = 'left';

	groupText.addChild(text);
	groupText.addChild(editableText);
	return groupText;
}

export default makeInput;
