/*
Paul Fitzgerald
evtuhovdo
https://stackoverflow.com/questions/32553158/detect-click-outside-react-component
 */

import { useState, useEffect, useRef } from 'react';

const useComponentVisible = (initialIsVisible: boolean) => {
	const [ isComponentVisible, setIsComponentVisible ] = useState(initialIsVisible);
	const ref = useRef(null);

	const handleClickOutside = (event: MouseEvent): void => {
		// @ts-ignore: Object is possibly 'null'.
		if (ref.current && !(ref?.current?.contains(event.target))) {
			setIsComponentVisible(false);
		}
	};

	useEffect(() => {
		document.addEventListener('click', handleClickOutside, true);
		return () => {
			document.removeEventListener('click', handleClickOutside, true);
		}
	});

	return { ref, isComponentVisible, setIsComponentVisible };
}

export default useComponentVisible;
