import Paper from 'paper';
import PaperColors from './PaperColors';

const MarkerWithText = (center: paper.Point, text: String) => {
	return new Paper.Group([
		new Paper.Path.Circle({
			center: center,
			radius: 3,
			fillColor: PaperColors.Red,
		}),
		new Paper.PointText({
			point: center.subtract(12),
			content: text,
			fillColor: PaperColors.Black,
			fontSize: 12,
		})
	]);
};

export default MarkerWithText;
