import Arrow from '../utils/Arrow';
import Paper from 'paper';
import PaperColors from './PaperColors';

export enum BoxSide {
	TOP,
	BOTTOM,
	LEFT,
	RIGHT
}

const drawBar = (from: paper.Point, to: paper.Point) => {
	const bar = new Paper.Path.Line(from, to);
	bar.strokeWidth = 3;
	bar.strokeColor = PaperColors.Black;
	return bar;
}

const drawValue = (point: paper.Point, value: string) => {
	const content = new Paper.PointText(point);
	content.content = value;
	content.fillColor = PaperColors.Black;
	content.fontSize = 18;
	content.fontWeight = 'bold';
	content.justification = 'center';
	return content;
}

const drawBox = (contourPath: paper.Path, boxSide: BoxSide, boxLabel: string, values: string[]) => {
	const boxOffset = 100;
	const boxWidth = 150;
	const labelSectionWidth = 30;
	const boxHeight = 30 * values.length;

	let referencePoint: paper.Point;

	switch (boxSide) {
		case BoxSide.TOP:
			referencePoint = contourPath.bounds.topCenter.clone();
			referencePoint.y -= boxOffset + boxHeight;
			referencePoint.x -= boxWidth / 2;
			break;
		case BoxSide.BOTTOM:
			referencePoint = contourPath.bounds.bottomCenter.clone();
			referencePoint.y += boxOffset;
			referencePoint.x -= boxWidth / 2;
			break;
		case BoxSide.LEFT:
			referencePoint = contourPath.bounds.leftCenter.clone();
			referencePoint.x -= boxOffset + boxWidth;
			break;
		case BoxSide.RIGHT:
			referencePoint = contourPath.bounds.rightCenter.clone();
			referencePoint.x += boxOffset;
			break;
	}

	const startPoint = referencePoint.clone();
	startPoint.y -= boxHeight / 2;

	const endPoint = referencePoint.clone();
	endPoint.y += boxHeight / 2;
	endPoint.x += boxWidth;

	const group = new Paper.Group();

	const borderBox = new Paper.Path.Rectangle(startPoint, endPoint);
	borderBox.strokeWidth = 3;
	borderBox.strokeColor = PaperColors.Black;
	group.addChild(borderBox);

	switch (boxSide) {
		case BoxSide.TOP:
			group.addChild(Arrow(borderBox.bounds.bottomCenter, contourPath.bounds.topCenter.clone(), false));
			break;
		case BoxSide.BOTTOM:
			group.addChild(Arrow(borderBox.bounds.topCenter, contourPath.bounds.bottomCenter.clone(), false));
			break;
		case BoxSide.LEFT:
			group.addChild(Arrow(borderBox.bounds.rightCenter, contourPath.bounds.leftCenter.clone(), false));
			break;
		case BoxSide.RIGHT:
			group.addChild(Arrow(borderBox.bounds.leftCenter, contourPath.bounds.rightCenter.clone(), false));
			break;
	}

	const topBarPoint = borderBox.bounds.topLeft.clone();
	topBarPoint.x += labelSectionWidth;
	const bottomBarPoint = borderBox.bounds.bottomLeft.clone();
	bottomBarPoint.x += labelSectionWidth;


	const bar = drawBar(topBarPoint, bottomBarPoint);
	group.addChild(bar);

	const labelPointOffset = borderBox.bounds.leftCenter.getDistance(bar.bounds.center) / 2;

	const labelPoint = borderBox.bounds.leftCenter.clone();
	labelPoint.x += labelPointOffset;
	labelPoint.y += 5;

	const label = drawValue(labelPoint, boxLabel);
	group.addChild(label);

	// values
	if (values.length > 1) {
		let leftBarPoint = topBarPoint.clone();
		let rightBarPoint = borderBox.bounds.topRight.clone();
		for (let i = 0; i < values.length; i++) {
			leftBarPoint.y += boxHeight / values.length;
			rightBarPoint.y += boxHeight / values.length;
			const __nextBar = drawBar(leftBarPoint, rightBarPoint);
			const centerRowPoint = leftBarPoint.clone();
			centerRowPoint.x += leftBarPoint.getDistance(rightBarPoint) / 2;
			centerRowPoint.y -= 5;
			group.addChild(__nextBar);
			drawValue(centerRowPoint, values[i])
		}
	} else {
		const contentPointOffset = bar.bounds.center.getDistance(borderBox.bounds.rightCenter) / 2;
		const contentPoint = bar.bounds.center.clone();
		contentPoint.x += contentPointOffset;
		contentPoint.y += 5;

		const content = drawValue(contentPoint, values[0]);
		group.addChild(content);
	}

	return group;
}

export default drawBox;
