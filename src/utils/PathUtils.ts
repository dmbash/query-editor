import Paper from 'paper';

export const HasOneCommonSegment = (...segments: paper.Segment[]) => {
	const seenValues: Array<paper.Segment> = [];
	let duplicateCount: number = 0;
	let commonSegment: paper.Segment | undefined;

	for (var i = 0; i < segments.length; i++) {
		const currentValue = segments[i];
		
		if (seenValues.find((value => currentValue == value))) {
			duplicateCount += 1;
			commonSegment = currentValue;
		}
		seenValues.push(currentValue);
	}

	return {
		duplicates: duplicateCount,
		common: commonSegment,
	}
}

export const getRelativeCurvePoint = (anchorPoint: paper.Segment, curve: paper.Curve, offset: number): paper.Point => {
	const multiplier = (anchorPoint != curve.segment2) ? -1 : 1;
	const newPoint = curve.getLocationAt((multiplier * curve.length) + ((multiplier * -1) * offset)).point;
	return newPoint;
};

