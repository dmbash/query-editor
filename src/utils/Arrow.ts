import Paper from 'paper';
import PaperColors from '../utils/PaperColors';

const makeArrow = (from: paper.Point, to: paper.Point, dual: boolean = true) => {
	const arrowAngle = 20;
	const arrowLength = 20;

	const makeArrowHead = (point: paper.Point, arrowVector: paper.Point) => {
		const nearPoint = point.add(arrowVector.normalize(arrowLength - 5));
		const res = new Paper.Path([nearPoint, point.add(arrowVector).rotate(arrowAngle, point), point, point.add(arrowVector).rotate(-arrowAngle, point), nearPoint]);
		res.fillColor = PaperColors.Black;
		return res;
	}


	const firstArrowHead = makeArrowHead(to, from.subtract(to).normalize(arrowLength));


	const arrowGroup = new Paper.Group([
		firstArrowHead,
		new Paper.Path([from, to]),
	]);

	if (dual) {
		const secondArrowHead = makeArrowHead(from, to.subtract(from).normalize(arrowLength));
		arrowGroup.addChild(secondArrowHead);
	}

	arrowGroup.strokeWidth = 3;
	arrowGroup.strokeColor = PaperColors.Black;
	return arrowGroup;
}

export default makeArrow;
