import AppDatabase from '../AppDatabase';
import BevelTool from "../tools/BevelTool";
import CurveTool from '../tools/CurveTool';
import DummyTool from '../tools/DummyTool';
import EllipseTool from '../tools/EllipseTool';
import LineTool from '../tools/LineTool';
import PointTool from '../tools/PointTool';
import RectangleTool from '../tools/RectangleTool';
import RoundTool from "../tools/RoundTool";
import SelectTool from '../tools/SelectTool';
import SelectionHandler from './SelectionHandler';
import store from '../store';
import type IApplicable from '../tools/IApplicable';
import type Tool from '../tools/Tool';
import { select, setInputValue, setToolInputFields } from '../store/ToolSlice';

interface ToolsList {
	BevelTool: BevelTool,
	CurveTool : CurveTool,
	EllipseTool: EllipseTool,
	LineTool: LineTool,
	PointTool: PointTool,
	RectangleTool: RectangleTool,
	RoundTool: RoundTool,
	SelectTool: SelectTool,
	DummyTool: DummyTool,
}

export default class ToolsHandler {
	toolsList: ToolsList;
	currentTool?: Tool;

	selectionHandler: SelectionHandler;
	appDb: AppDatabase;
	onInputRequest: Function;
	onInputApplied: Function;
	
	constructor(appDb: AppDatabase, selectionHandler: SelectionHandler, onInputRequest: Function, onInputApplied: Function) {
		this.appDb = appDb;
		this.selectionHandler = selectionHandler;
		this.onInputRequest = onInputRequest;
		this.onInputApplied = onInputApplied;
		
		this.toolsList = {
			BevelTool: new BevelTool(this),
			CurveTool: new CurveTool(this),
			EllipseTool: new EllipseTool(this),
			LineTool: new LineTool(this),
			PointTool: new PointTool(this),
			RectangleTool: new RectangleTool(this),
			RoundTool: new RoundTool(this),
			SelectTool: new SelectTool(this),
			DummyTool: new DummyTool(this),
		};

		this.toolsList.SelectTool.activate();
	}

	isApplicable(tool: any): tool is IApplicable {
		return 'apply' in tool;
	}

	onActivate(activatingTool: Tool) {
		store.dispatch(select(
			{
				name: activatingTool.getName(),
				localizedName: activatingTool.getLocalizedName(),
				needInputBar: activatingTool.getNeedInputBar(),
		}));
		if (activatingTool.getNeedInputBar()) {
			store.dispatch(setToolInputFields(activatingTool.getInputFields()));
		}
	}

	onApplied(appliedTool: Tool) {
		this.onInputApplied();
	}

	activateToolByName(name: string): void {
		let found = false;

		let key: keyof ToolsList;
		for (key in this.toolsList) {
			if (Object.prototype.hasOwnProperty.call(this.toolsList, key)) {
				const toolByKey = this.toolsList[key]
				if (name === toolByKey.getName()) {
					toolByKey.activate();
					return;
				}
			}
		}

		if (!found) {
			this.toolsList.SelectTool.activate();
		}
	}
}
