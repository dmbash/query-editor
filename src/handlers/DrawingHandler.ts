import Paper from 'paper';

export default class DrawingHandler {
	canvas: HTMLCanvasElement;

	constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
	}
}

