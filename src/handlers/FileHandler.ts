import AppDatabase from "../AppDatabase";
import DrawingContour from '../dto/DrawingContour';

export default class FileHandler {

	appDb: AppDatabase

	constructor(appDb: AppDatabase) {
		this.appDb = appDb;
	}
	
	saveToFile() {
		const contours = this.appDb.getContours();
		const serializedContours = [];
		if (contours.length > 0) {
			for (var i = 0; i < contours.length; i++) {
				serializedContours.push(DrawingContour.serialize(contours[i]));
			}
		}
		const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(serializedContours));
		const anchorNode = document.createElement('a');
		anchorNode.setAttribute("href", dataStr);
		anchorNode.setAttribute("download", Date.now().toString() + ".json");
		document.body.appendChild(anchorNode);
		anchorNode.click();
		anchorNode.remove();
	}

	loadFromFile(file: File) {
		// FIXME: implement
		// deserialize array, then call DrawingContour.desearilize, then add to appDb
	}
}
