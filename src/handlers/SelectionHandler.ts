import DrawingContour from "../dto/DrawingContour";
import { setHasSelectedContour } from '../store/ToolSlice';
import store from '../store';
import PaperColors from "../utils/PaperColors";

export default class SelectionHandler {
	private selectedContour: DrawingContour | null = null;
	private selectedSegment: paper.Segment | null = null;
	private selectedType: String | null = null;

	select(selectedContour: DrawingContour, selectedSegment: paper.Segment, selectedType: String) {
		this.deselectAll();
		
		this.selectedContour = selectedContour;
		this.selectedContour.path.selected = true;
		this.selectedContour.path.strokeColor = PaperColors.Yellow;

		this.selectedType = selectedType;
		
		this.selectedSegment = selectedSegment;
		if (this.selectedType === 'segment' || this.selectedType === 'handle-in' || this.selectedType === 'handle-out') {
			this.selectedSegment.selected = true;
		}
		store.dispatch(setHasSelectedContour(true));
	}

	getSelectedContourId(): number | null {
		if (this.hasSelectedContour()) {
			return this.selectedContour!.id;
		} else {
			return null;
		}
	}

	deselectAll() {
		this.deselectContour();
		this.deselectSegment();
		this.selectedType = null;
	}

	deselectContour() {
		if (this.selectedContour) {
			this.selectedContour.path.selected = false;
			this.selectedContour.path.strokeColor = this.selectedContour.getRolePathColor();
			this.selectedContour = null;
			store.dispatch(setHasSelectedContour(false));
		}
	}

	deselectSegment() {
		if (this.selectedSegment) {
			this.selectedSegment.selected = false;
			this.selectedSegment = null;
		}
	}

	hasSelectedContour() {
		return this.selectedContour != null;
	}

	hasSelectedSegment() {
		return this.selectedSegment != null;
	}

	getSelectedType() {
		return this.selectedType;
	}

	getSelectedContour(): DrawingContour | null {
		return this.selectedContour;
	}

	getSelectedSegment(): paper.Segment | null {
		return this.selectedSegment;
	}
	
	
}
