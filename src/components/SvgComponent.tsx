import { ReactNode } from 'react';

type SvgComponentProps = {
	isActive: boolean,
	children: ReactNode,
	onClick: Function,
};

const SvgComponent = ({isActive, children, onClick} : SvgComponentProps) => {

	let className = "Svg";

	if (isActive) {
		className += " Svg--Active";
	}
	
	return (
		<div className={className} onClick={() => onClick()}>
			{ children }
		</div>
	);
};

export default SvgComponent;
