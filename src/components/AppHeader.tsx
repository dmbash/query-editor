import TopMenu from './TopMenu';
import Toolbar from './Toolbar';
import ToolInputBar from './ToolInputBar';
import { useAppSelector } from '../hooks';

type HeaderProps = {
	onToolApply: Function,
}

const Header = ({onToolApply}: HeaderProps) => {
	
	const needInputBar = useAppSelector(state => state.tool.needInputBar);
	const currentToolLocalizedName = useAppSelector(state => state.tool.localizedName);

	const getToolbarLabel = () => {
		return 'Ввод данных для инструмента ' + currentToolLocalizedName + ':';
	}

	return (
		<header>
			<TopMenu />
			<Toolbar />
			{needInputBar &&
				<ToolInputBar onApply={onToolApply} label={getToolbarLabel()} />
			}
		</header>
	);
}

export default Header;
