import Legend from './Legend';
import ScaleView from './ScaleView';
import StatusBar from './StatusBar';

const AppFooter = () => {
	return (
		<footer>
			<div className="PreStatusBarContainer">
				<Legend />
				<ScaleView />
			</div>
			<StatusBar />
		</footer>
	);
}

export default AppFooter;
