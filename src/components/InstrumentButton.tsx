import React from 'react';
import './InstrumentButton.scss';

export enum ToolButtonState {
	DEFAULT,
	ACTIVE,
	DISABLED,
}

export interface ToolButtonProps {
	icon: String;
	localizedName: String;
	clickCallback: Function;
	state: ToolButtonState;
}

function ToolButton(props : ToolButtonProps) {
	const iconClassName = `InstrumentButton InstrumentButton--${props.icon}`;
	let containerClassName = 'InstrumentButton';
	if (props.state === ToolButtonState.ACTIVE) {
		containerClassName += ' InstrumentButton--active';
	} else if (props.state === ToolButtonState.DISABLED) {
		containerClassName += ' InstrumentButton--disabled';
	}
	return (
		<div onClick={() => props.clickCallback(props.icon) } className={containerClassName}>
			<div className={iconClassName}/>
		</div>
	)
}

ToolButton.defaultProps = {
	state: ToolButtonState.DEFAULT,
	clickCallback: () => {},
};

export default ToolButton;
