import './StatusBar.scss';
import { useAppSelector } from '../hooks';

const StatusBar = () => {

	const currentToolName = useAppSelector(state => state.tool.localizedName);

	return (
		<div className="StatusBar">
			{ currentToolName &&
				'Текущий инструмент - "' + currentToolName + '"'
			}
		</div>
	);
}

export default StatusBar;

