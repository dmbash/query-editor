import './HoverMenu.scss';

type HoverMenuProps = {
	x: number,
	y: number,
	children: React.ReactNode,
}

const HoverMenu = ({x, y, children}: HoverMenuProps) : JSX.Element=> {

	const styles = {
		left: x,
		top: y,
	};

	return (
		<div className="HoverMenuWrapper" style={styles}>
			{children}
		</div>
	);
}

export default HoverMenu;
