import './HoverToolbar.scss';

export interface HoverToolbarProps {
	x: number,
	y: number,
}

const HoverToolbar = (props: HoverToolbarProps) => {

	const containerStyles = {
		left: props.x,
		top: props.y,
	}

	return (
		<div className="HoverToolbar" style={containerStyles}>
			HoverToolbar
		</div>
	);
}

export default HoverToolbar;
