import React from 'react';
import './TopMenu.scss';
import {
	Menu,
	MenuItem,
	MenuButton,
	ClickEvent
} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import { AppContext, IAppContext } from '../App';
import { useContext } from 'react';

function TopMenu(): JSX.Element {

	const appCtx: IAppContext | null = useContext(AppContext);

	const onDrawingSaveButtonClick = (e: ClickEvent) => {
		const fileHandler = appCtx!.getFileHandler();
		fileHandler.saveToFile();
	}

	const onDrawingLoadButtonClick = (e : ClickEvent) => {
		console.log("load");
	}
		
	return (
		<div className="TopMenu">
			<Menu className="MenuContainer" menuButton={<MenuButton className="TopMenuButton">Файл</MenuButton>}>
				<MenuItem onClick={onDrawingSaveButtonClick}>Сохранить чертёж-запрос</MenuItem>
				<MenuItem onClick={onDrawingLoadButtonClick}>Загрузить чертёж-запрос</MenuItem>
			</Menu>
			<Menu className="MenuContainer" menuButton={<MenuButton className="TopMenuButton">СГПЧ</MenuButton>}>
			</Menu>
			<Menu className="MenuContainer" menuButton={<MenuButton className="TopMenuButton">Справка</MenuButton>}>
			</Menu>
		</div>
	)
}

export default TopMenu;
