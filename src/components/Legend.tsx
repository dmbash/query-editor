import './Legend.scss';

import BasicLine from '../assets/legend_icons/basic_line.svg';
import BaseLine from '../assets/legend_icons/base_line.svg';
import SelectedLine from '../assets/legend_icons/selected_line.svg';
import RequiredLine from '../assets/legend_icons/required_line.svg';
import Centroid from '../assets/legend_icons/centroid.svg';
import CircleMarker from '../assets/legend_icons/circle_marker.svg';
import Cursor from '../assets/legend_icons/cursor.svg';
import SelectPointCursor from '../assets/legend_icons/select_point_cursor.svg';
import ToolCursor from '../assets/legend_icons/tool_cursor.svg';

const Legend = () => {
	return (
		<div className="LegendContainer">
			<div className="LegendHeader">
				Легенда
			</div>
			<div className="LegendContent">
				<div className="LegendEntry">
					<img src={BasicLine} alt="Контур" />
					Контур
				</div>
				<div className="LegendEntry">
					<img src={SelectedLine} alt="Выделенный контур" />
					Выделенный контур
				</div>
				<div className="LegendEntry">
					<img src={RequiredLine} alt="Обязательный контур" />
					Обязательный контур
				</div>
				<div className="LegendEntry">
					<img src={BaseLine} alt="Базовый контур" />
					Базовый контур
				</div>
				<div className="LegendEntry">
					<img src={Centroid} alt="Центроид" />
					Центроид
				</div>
				<div className="LegendEntry">
					<img src={CircleMarker} alt="Метка отрезка контура в режиме фаски/скругления" />
					Метка отр. контура в режиме фаски/скругления
				</div>
				<div className="LegendEntry">
					<img src={Cursor} alt="Курсор" />
					Курсор
				</div>
				<div className="LegendEntry">
					<img src={ToolCursor} alt="Курсор активного инструмента" />
					Курсор активного инструмента
				</div>
				<div className="LegendEntry">
					<img src={SelectPointCursor} alt="Курсор точки выделения" />
					Курсор точки выделения
				</div>
			</div>
		</div>
	);
}

export default Legend;
