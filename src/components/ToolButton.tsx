import { useContext } from 'react';
import { ToolbarContext } from './Toolbar';
import './ToolButton.scss';

export interface ToolButtonProps {
	name: String;
	disabled: boolean;
}

const ToolButton = (props: ToolButtonProps) => {

	const ctx = useContext(ToolbarContext);

	function handleClick() {
		if (props.disabled) {
			return;
		}
		notifyParentOnClick();
	}

	function notifyParentOnClick() {
		if (ctx && ctx.onToolSelect) {
			ctx.onToolSelect(props.name)
		}
	}

	const iconClassName = `ToolButton ToolButton--${props.name}`;
	let containerClassName = 'ToolButton';

	if (ctx && ctx.isSelected(props.name)) {
		containerClassName += ' ToolButton--active';
	} else if (props.disabled) {
		containerClassName += ' ToolButton--disabled';
	}

	return (
		<div onClick={() => handleClick()} className={containerClassName}>
			<div className={iconClassName} />
		</div>
	);

}

ToolButton.defaultProps = {
	disabled: false,
}

export default ToolButton;

