import { ReactNode } from 'react';
import { CSSProperties } from 'react';
import './AppModal.scss';
import ReactModal from 'react-modal';
import Draggable from 'react-draggable';

interface Styles {
	[key: string]: CSSProperties,
}

type AppModalProps = {
	isOpen: boolean,
	children: ReactNode,
	handle?: string,
	afterOpen: Function,
};

const AppModal = ({isOpen, children, handle, afterOpen}: AppModalProps) => {

	const styleOverrides: Styles = {
		content: {
			overflow: "visible",
			padding: 0,
			border: "none",
			borderRadius: 0,
			top: '50%',
			left: '50%',
			bottom: 'auto',
			right: 'auto',
			transform: 'translate(-45%, -50%)',
			background: "none",
			position: 'absolute',
		}
	};

	return (
		<ReactModal
			style={styleOverrides}
			isOpen={isOpen}
			onAfterOpen={() => afterOpen()}
		>
			<Draggable handle={handle}>
				{ children }
			</Draggable>
		</ReactModal >
	);
}

export default AppModal;
