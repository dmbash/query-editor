import './ToolInputBar.scss';
import { setInputValue, ToolInputField } from '../store/ToolSlice';
import { useAppDispatch, useAppSelector } from '../hooks';

export interface ToolInputBarProps {
	label: String,
	onApply: Function,
}

const ToolInputBar = (props: ToolInputBarProps) => {

	const dispatch = useAppDispatch();
	const inputFields: Array<ToolInputField> = useAppSelector(state => state.tool.inputFields);

	return (
		<div className="ToolInputBar">
			<span className="ToolInputBar--Label">
				{props.label}
			</span>
			{
				inputFields.map((field, index) => {
					return (
						<label key={index} className="ToolInputBarField--Label">
							{field.label}
							<input
								className="ToolInputBarField--Input"
								type="number"
								name={field.fieldName.toString()}
								defaultValue={field.defaultValue}
								value={field.value}
								onChange={(e) => dispatch(setInputValue({
									newValue: Number.parseInt(e.target.value),
									fieldName: field.fieldName,
								}))}
							/>
						</label>
					);
				})
			}
			<button className="ToolInputBar--Button" onClick={e => props.onApply()}>Применить</button>
		</div>
	);
}

export default ToolInputBar;
