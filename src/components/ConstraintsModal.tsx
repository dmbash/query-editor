import './AppModal';
import './ConstraintsModal.scss';
import AppModal from './AppModal';
import ConstraintsRow from './ConstraintsRow';
import DrawingContour, { ContourRole } from '../dto/DrawingContour';
import GSizeDraw from '../constraints/GSize';
import ISpecialConstraint, { SCRelValue } from '../constraints/ISpecialConstraint';
import IsRightClicked from '../utils/IsRightClicked';
import Paper from 'paper';
import SCAspRatio from '../constraints/SCAspRatio';
import SCLoc from '../constraints/SCLoc';
import SCOrient from '../constraints/SCOrient';
import SCShape from '../constraints/SCShape';
import SCSize from '../constraints/SCSize';
import type SelectionHandler from '../handlers/SelectionHandler';
import { AppContext } from '../App';
import { ReactComponent as Asp1 } from '../assets/constraints/asp/1.svg';
import { ReactComponent as Asp2 } from '../assets/constraints/asp/2.svg';
import { ReactComponent as Asp3 } from '../assets/constraints/asp/3.svg';
import { ReactComponent as Loc1 } from '../assets/constraints/loc/1.svg';
import { ReactComponent as Loc2 } from '../assets/constraints/loc/2.svg';
import { ReactComponent as Loc3 } from '../assets/constraints/loc/3.svg';
import { ReactComponent as Orient1 } from '../assets/constraints/orient/1.svg';
import { ReactComponent as Orient2 } from '../assets/constraints/orient/2.svg';
import { ReactComponent as Orient3 } from '../assets/constraints/orient/3.svg';
import { ReactComponent as Shape1 } from '../assets/constraints/shape/1.svg';
import { ReactComponent as Shape2 } from '../assets/constraints/shape/2.svg';
import { ReactComponent as Shape3 } from '../assets/constraints/shape/3.svg';
import { ReactComponent as Size1 } from '../assets/constraints/size/1.svg';
import { ReactComponent as Size2 } from '../assets/constraints/size/2.svg';
import { ReactComponent as Size3 } from '../assets/constraints/size/3.svg';
import { drawSettings as GAspRatioDraw } from '../constraints/GAspRatio';
import { drawSettings as GLocDraw } from '../constraints/GLoc';
import { drawSettings as GOrientDraw } from '../constraints/GOrient';
import { useState, useContext, useCallback } from 'react';

type ConstraintsModalProps = {
	isOpen: boolean,
	selectionHandler: SelectionHandler;
};

type ConstraintOption = {
	limit: number | number[],
	relValue?: SCRelValue,
	units?: string | string[],
	value: number | number[],
}

type ConstraintsOptions = {
	aspRatio: ConstraintOption,
	loc: ConstraintOption,
	orient: ConstraintOption,
	shape: ConstraintOption,
	size: ConstraintOption,
}

const getInitialOptions = () => {
	const obj = {
		aspRatio: {
			value: 0,
			limit: 0,
			relValue: undefined,
			units: '*',
		},
		loc: {
			value: [0, 0],
			limit: [0, 0],
			relValue: undefined,
			units: ['*', 'мм'],
		},
		orient: {
			value: 0,
			limit: 0,
			relValue: undefined,
			units: '*',
		},
		shape: {
			value: 0,
			limit: 0,
			relValue: undefined,
		},
		size: {
			value: [0, 0],
			limit: [0, 0],
			relValue: undefined,
			units: ['мм', 'мм'],
		}
	}
	return {...obj};
}

// TODO: add ability to input limits

const ConstraintsModal = (props: ConstraintsModalProps) => {

	const [isHidden, setIsHidden] = useState<Boolean>(false);
	const [constraintsOptions, setConstraintsOptions] = useState<ConstraintsOptions>(getInitialOptions());

	const resetConstraintsOptions = useCallback(() => {
		setConstraintsOptions(getInitialOptions);
	}, [getInitialOptions()]);
	
	const [currentRole, setCurrentRole] = useState<ContourRole>(ContourRole.REQUIRED);

	const handleToggler = (newState: ContourRole) => {
		if (currentRole === ContourRole.DEFAULT && newState === ContourRole.DEFAULT) {
			setCurrentRole(ContourRole.REQUIRED);
		} else if (currentRole === ContourRole.REQUIRED && newState === ContourRole.DEFAULT) {
			setCurrentRole(ContourRole.DEFAULT);
		} else if (currentRole === ContourRole.BASE && newState === ContourRole.DEFAULT) {
			setCurrentRole(ContourRole.REQUIRED);
		} else if (currentRole === ContourRole.BASE && newState === ContourRole.BASE) {
			setCurrentRole(ContourRole.DEFAULT);
		} else {
			setCurrentRole(ContourRole.BASE);
		}
	}

	const appCtx = useContext(AppContext);

	const resetState = () => {
		resetConstraintsOptions();
		setCurrentRole(ContourRole.REQUIRED);
	}

	const handleOkClick = () => {
		const contour: DrawingContour | null = props.selectionHandler.getSelectedContour();

		if (!contour)
			return;

		{
			contour.specialConstraints.shape = new SCShape(constraintsOptions.shape.relValue);
		}

		{
			const [width, height] = constraintsOptions.size.value as number[];
			const [widthLimit, heightLimit] = constraintsOptions.size.limit as number[];
			let widthUnit = 'мм';
			let heightUnit = 'мм';
			if (constraintsOptions.size.units) {
				widthUnit = constraintsOptions.size.units[0];
				heightUnit = constraintsOptions.size.units[1];
			}
			contour.specialConstraints.size = new SCSize([width, height], [widthLimit, heightLimit], constraintsOptions.size.relValue, widthUnit);
		}

		{
			const { value: arVal, limit: arLim, relValue: arRelVal } = constraintsOptions.aspRatio;
			contour.specialConstraints.aspRatio = new SCAspRatio(arVal as number, arLim as number, arRelVal);
		}

		{
			const { value: orVal, limit: orLim, relValue: orRelVal } = constraintsOptions.orient;
			contour.specialConstraints.orient = new SCOrient(orVal as number, orLim as number, orRelVal);
		}

		{
			const [angleVal, lengthVal] = constraintsOptions.loc.value as number[];
			const [angleLim, lengthLim] = constraintsOptions.loc.limit as number[];
			const locRelValue = constraintsOptions.loc.relValue;
			contour.specialConstraints.loc = new SCLoc([angleVal, lengthVal], [angleLim, lengthLim], locRelValue);
		}

		contour.specialConstraints.contourRole = currentRole;

		resetState();
		appCtx!.constraintsFunctions.onOk();
	};
	const handleCancelClick = () => {
		resetState();
		appCtx!.constraintsFunctions.onCancel();
	};

	const afterOpen = () => {
		/*
			 modal just opened, so we need to make sure, that selected contour has specialConstraints object initialized,
			 then we can manipulate with these values
		*/
	
		const contour: DrawingContour | null = props.selectionHandler.getSelectedContour();
		if (contour) {
			const constraintsOptionsClone = { ...constraintsOptions };

			const currentAspRatioVal = contour.getAspRatio();
			const currentWidthVal = contour.getWidth();
			const currentHeightVal = contour.getHeight();
			const currentOrientVal = contour.getOrientation();
			// FIXME should be really centroid, not canvas center
			const { angle: currentLocationAngleVal, length: currentLocationLengthVal } = contour.getLocation(Paper.view.center);

			constraintsOptionsClone.aspRatio.value = currentAspRatioVal;
			constraintsOptionsClone.size.value = [currentWidthVal, currentHeightVal];
			constraintsOptionsClone.orient.value = currentOrientVal;
			constraintsOptionsClone.loc.value = [currentLocationAngleVal, currentLocationLengthVal];

			const { aspRatio: cAspRatio, size: cSize, shape: cShape, loc: cLoc, orient: cOrient, contourRole: cContourRole } = contour.specialConstraints;

			const setUpConstraint = (constraint: ConstraintOption, newConstraint: ISpecialConstraint) => {
				constraint.value = newConstraint.value;
				constraint.limit = newConstraint.limit;
				constraint.relValue = newConstraint.relValue;
				constraint.units = newConstraint.units;
			}

			if (cShape) {
				setUpConstraint(constraintsOptionsClone.shape, cShape);
			}


			if (cSize) {
				setUpConstraint(constraintsOptionsClone.size, cSize);
			}

			if (cAspRatio) {
				setUpConstraint(constraintsOptionsClone.aspRatio, cAspRatio);
			}

			if (cOrient) {
				setUpConstraint(constraintsOptionsClone.orient, cOrient);
			}

			if (cLoc) {
				setUpConstraint(constraintsOptionsClone.loc, cLoc);
			}

			setCurrentRole(cContourRole);

			setConstraintsOptions({ ...constraintsOptionsClone });
		}
	};

	const onSizeSettingClick = () => {
		setIsHidden(true);
		const contour = props.selectionHandler.getSelectedContour();
		if (contour!.specialConstraints.size && contour!.specialConstraints.size.marker) {
			contour!.specialConstraints.size.marker.clear!();
		}
		
		const ret = GSizeDraw(contour!);
		
		Paper.view.onClick = (e: paper.MouseEvent) => {
			if (IsRightClicked(e)) {
				setIsHidden(false);
				const limit = [Number.parseInt(ret.widthContent()), Number.parseInt(ret.heightContent())];

				// FIXME: specialConstraints don't exist yet, find the way to fix
				if (contour!.specialConstraints.size) {
					contour!.specialConstraints.size.limit = limit;
				} else {
					contour!.specialConstraints.size = new SCSize([contour!.getWidth(), contour!.getHeight()], limit);
				}
				ret.clear();
				Paper.view.onClick = null;
			}
		}
	};

	const onAspSettingClick = () => {
		setIsHidden(true);

		const contour = props.selectionHandler.getSelectedContour();
		if (contour!.specialConstraints.aspRatio && contour!.specialConstraints.aspRatio.marker) {
			contour!.specialConstraints.aspRatio.marker.clear!();
		}

		const ret = GAspRatioDraw(contour!);

		Paper.view.onClick = (e: paper.MouseEvent) => {
			if (IsRightClicked(e)) {
				setIsHidden(false);
				const limit = Number.parseInt(ret.aspLimit());

				if (contour!.specialConstraints.aspRatio) {
					contour!.specialConstraints.aspRatio.limit = limit;
				} else {
					contour!.specialConstraints.aspRatio = new SCAspRatio(contour!.getAspRatio(), limit);
				}
				
				ret.clear();
				Paper.view.onClick = null;
			}
		}
	};

	const onLocationSettingClick = () => {
		setIsHidden(true);

		const contour = props.selectionHandler.getSelectedContour();
		if (contour!.specialConstraints.orient && contour!.specialConstraints.orient.marker) {
			contour!.specialConstraints.orient.marker.clear!();
		}

		const ret = GLocDraw(contour!);

		Paper.view.onClick = (e: paper.MouseEvent) => {
			if (IsRightClicked(e)) {
				setIsHidden(false);
				
				const angleLimit = Number.parseInt(ret.angleLimit());
				const lengthLimit = Number.parseInt(ret.lengthLimit());

				if (contour!.specialConstraints.loc) {
					contour!.specialConstraints.loc.limit[0] = angleLimit;
					contour!.specialConstraints.loc.limit[1] = lengthLimit
				} else {
					const loc = contour!.getLocation(Paper.view.center);
					contour!.specialConstraints.loc = new SCLoc([loc.angle, loc.length], [angleLimit, lengthLimit]);
				}
				
				ret.clear();
				Paper.view.onClick = null;
			}
		}
	};

	const onOrientSettingClick = () => {
		setIsHidden(true);

		const contour = props.selectionHandler.getSelectedContour();
		if (contour!.specialConstraints.loc && contour!.specialConstraints.loc.marker) {
			contour!.specialConstraints.loc.marker.clear!();
		}

		const ret = GOrientDraw(contour!);

		Paper.view.onClick = (e: paper.MouseEvent) => {
			if (IsRightClicked(e)) {
				setIsHidden(false);
				
				const limit = Number.parseInt(ret.orientLimit());

				if (contour!.specialConstraints.orient) {
					contour!.specialConstraints.orient.limit = limit;
				} else {
					contour!.specialConstraints.orient = new SCOrient(contour!.getOrientation(), limit);
				}
				
				ret.clear();
				Paper.view.onClick = null;
			}
		}
	};

	return (
		<AppModal afterOpen={afterOpen} isOpen={props.isOpen && !isHidden} handle=".handle">
			<div className="ModalContainer">
				<h5 className="handle">Задание специальных требований</h5>
				<div className="ConstraintsGrid">
					<div className="empty" />
					<div className="tochnoe GridHeader"> Точное соответствие </div>
					<div className="pribl GridHeader"> Приблизительное соответствие </div>
					<div className="zna4 GridHeader"> Может значительно отличаться </div>
					<div className="abs GridHeader"> Абсолютное значение </div>
					<ConstraintsRow
						label="Соответствие формы"
						relOptionValue={constraintsOptions.shape.relValue}
						exactSvg={<Shape1 />}
						approxSvg={<Shape2 />}
						signifSvg={<Shape3 />}
						onSelectRelValue={(newValue: SCRelValue) => {
							setConstraintsOptions({
								...constraintsOptions, shape: {
									...constraintsOptions.shape,
									relValue: newValue,
								}
							})
						}}
						onLimitInput={() => { }}
					/>
					<ConstraintsRow
						label="Соответствие размера"
						relOptionValue={constraintsOptions.size.relValue}
						exactSvg={<Size1 />}
						approxSvg={<Size2 />}
						signifSvg={<Size3 />}
						absValues={
							[{
								label: 'ширина',
								value: (constraintsOptions.size.value as number[])[0],
								limit: (constraintsOptions.size.limit as number[])[0],
								units: 'мм',
							},
							{
								label: 'высота',
								value: (constraintsOptions.size.value as number[])[1],
								limit: (constraintsOptions.size.limit as number[])[1],
								units: 'мм',
							}]
						}
						onLimitInput={(newValue: string, index: number) => {
							const clone = { ...constraintsOptions };
							(clone.size.limit as number[])[index] = Number.parseFloat(newValue);
							setConstraintsOptions({ ...clone });
						}}
						onSelectRelValue={(newValue: SCRelValue) => {
							setConstraintsOptions({
								...constraintsOptions, size: {
									...constraintsOptions.size,
									relValue: newValue,
								}
							})
						}}
						onSettingsClick={onSizeSettingClick}
					/>
					<ConstraintsRow
						label="Соотношение сторон"
						relOptionValue={constraintsOptions.aspRatio.relValue}
						exactSvg={<Asp1 />}
						approxSvg={<Asp2 />}
						signifSvg={<Asp3 />}
						onLimitInput={(newValue: number, index: number) => {
							const clone = { ...constraintsOptions };
							(clone.aspRatio.limit as number) = newValue;
							setConstraintsOptions({ ...clone });
						}}
						absValues={
							[{
								label: 'коэффициент',
								value: (constraintsOptions.aspRatio.value) as number,
								limit: (constraintsOptions.aspRatio.limit) as number,
								units: '%',
							}]
						}
						onSelectRelValue={(newValue: SCRelValue) => {
							setConstraintsOptions({
								...constraintsOptions, aspRatio: {
									...constraintsOptions.aspRatio,
									relValue: newValue,
								}
							})
						}}
						onSettingsClick={onAspSettingClick}
					/>
					<ConstraintsRow
						label="Ориентация"
						relOptionValue={constraintsOptions.orient.relValue}
						exactSvg={<Orient1 />}
						approxSvg={<Orient2 />}
						signifSvg={<Orient3 />}
						onLimitInput={(newValue: number, index: number) => {
							const clone = { ...constraintsOptions };
							(clone.orient.limit as number) = newValue;
							setConstraintsOptions({ ...clone });
						}}
						absValues={
							[{
								label: 'угол',
								value: (constraintsOptions.orient.value) as number,
								limit: (constraintsOptions.orient.limit) as number,
								units: '°',
							}]
						}
						onSelectRelValue={(newValue: SCRelValue) => {
							setConstraintsOptions({
								...constraintsOptions, orient: {
									...constraintsOptions.orient,
									relValue: newValue,
								}
							})
						}}
						onSettingsClick={onOrientSettingClick}
					/>
					<ConstraintsRow
						label="Местоположение"
						relOptionValue={constraintsOptions.loc.relValue}
						exactSvg={<Loc1 />}
						approxSvg={<Loc2 />}
						signifSvg={<Loc3 />}
						onLimitInput={(newValue: number, index: number) => {
							const clone = { ...constraintsOptions };
							(clone.loc.limit as number[])[index] = newValue;
							setConstraintsOptions({ ...clone });
						}}
						absValues={
							[{
								label: 'угол',
								value: (constraintsOptions.loc.value as number[])[0],
								units: '°',
								limit: (constraintsOptions.loc.limit as number[])[0],
							},
							{
								label: 'длина вектора',
								value: (constraintsOptions.loc.value as number[])[1],
								limit: (constraintsOptions.loc.limit as number[])[1],
								units: 'мм',

							}]
						}
						onSelectRelValue={(newValue: SCRelValue) => {
							setConstraintsOptions({
								...constraintsOptions, loc: {
									...constraintsOptions.loc,
									relValue: newValue,
								}
							})
						}}
						onSettingsClick={onLocationSettingClick}
					/>
				</div>
				<div className="ModalContainer-Divider" />
				<div className="ModalContainer-Toggles">
					<div className="TogglerContainer">
						<div>Контур может отсутствовать в целевом чертеже</div>
						<span className={`Toggler ${currentRole === ContourRole.DEFAULT ? "Toggler--on" : ""}`} onClick={() => handleToggler(ContourRole.DEFAULT)} />
					</div>
					<div className="TogglerContainer">
						<div>Базовый контур</div>
						<span className={`Toggler ${currentRole === ContourRole.BASE ? "Toggler--on" : ""}`} onClick={() => handleToggler(ContourRole.BASE)} />
					</div>
				</div>
				<div className="ModalContainer-Buttons">
					<button className="button" onClick={() => handleOkClick()}>ОК</button>
					<button className="button" onClick={() => handleCancelClick()}>Отмена</button>
				</div>
			</div >
		</AppModal >
	);
};

export default ConstraintsModal;
