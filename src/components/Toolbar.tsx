import { useContext, createContext } from "react";
import { AppContext, IAppContext } from '../App';
import ToolButton from './ToolButton';
import './Toolbar.scss';
import { useAppSelector } from '../hooks';
import ToolsHandler from "../handlers/ToolsHandler";

interface IToolbarContext {
	onToolSelect: Function,
	isSelected: Function,
}

export const ToolbarContext = createContext<IToolbarContext | null>(null);

export default function Toolbar() {
	const appCtx : IAppContext | null = useContext(AppContext);
	const currentTool = useAppSelector(state => state.tool);
	const hasContourSelected = useAppSelector(state => state.tool.hasSelectedContour);

	function onToolSelect(name: String): void {
		const toolsHandler: ToolsHandler = appCtx!.getToolsHandler();

		if (currentTool.name === name) {
			toolsHandler.currentTool!.abort();
			return;
		}

		if (name === 'constraints') {
			// open modal
			toolsHandler.activateToolByName('dummy');
			appCtx!.constraintsFunctions.onOpen();
			return;
		}

		toolsHandler.activateToolByName(name.toString());
	}

	function isSelected(name: String): boolean {
		if (currentTool) {
			return name === currentTool.name;
		}
		return false;
	}

	const ctx: IToolbarContext = {
		onToolSelect,
		isSelected,
	};

	return (
		<div className="toolbar">
			<ToolbarContext.Provider value={ctx}>
				<ToolButton
					name="point"
				/>
				<ToolButton
					name="line" />
				<ToolButton
					name="curve" />
				<ToolButton
					name="ellipse" />
				<ToolButton
					name="rectangle" />
				<ToolButton
					name="bevel"
				/>
				<ToolButton
					name="round"
				/>
				<div className="ToolbarVerticalBar" />
				<ToolButton
					name="linearsize"
					disabled={!hasContourSelected}
				/>
				<ToolButton
					name="radialsize"
					disabled={!hasContourSelected}
				/>
				<ToolButton
					name="constraints"
					disabled={!hasContourSelected}
				/>
				<div className="ToolbarVerticalBar" />
				<ToolButton
					name="biblio"
				/>
			</ToolbarContext.Provider>
		</div >
	);
}
