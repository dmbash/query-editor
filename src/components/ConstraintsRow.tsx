import { Fragment, ReactNode } from 'react';
import SvgComponent from './SvgComponent';
import { SCRelValue } from '../constraints/ISpecialConstraint';

type AbsValue = {
	label: String,
	value: number,
	limit: number,
	units?: String,
}

type ConstraintsRowProps = {
	label: String
	relOptionValue?: SCRelValue,
	exactSvg: ReactNode,
	approxSvg: ReactNode,
	signifSvg: ReactNode,
	absValues?: Array<AbsValue>,
	onSelectRelValue: Function,
	onSettingsClick?: Function,
	onLimitInput: Function,
};

const ConstraintsRow = (props: ConstraintsRowProps): JSX.Element => {

	let absValuesContent;

	const handleRelValueClick = (newValue: SCRelValue) => {
		const oldValue = props.relOptionValue;

		if (newValue === oldValue) {
			props.onSelectRelValue(null);
		} else {
			props.onSelectRelValue(newValue);
		}
	};

	if (props.absValues) {
		absValuesContent = props.absValues.map((absValue, index) => {
			return (
				<div className="AbsValue" key={index}>
					<span className="AbsValue--Label">{absValue.label}</span>
					<div className="AbsValue--Container">
						<span className="AbsValue--Value">{absValue.value}</span>
						<span className="AbsValue--PlusMinus" />
						<input className="AbsValue--LimitValue InputNumber" type="text" defaultValue={absValue.limit} value={absValue.limit} onChange={(e) => props.onLimitInput(e.target.value, index)}></input>
						{/* <span className="AbsValue--LimitValue">{absValue.limit}</span> */}
						{absValue.units && <span className="AbsValue--Value">{absValue.units}</span>}
					</div>
				</div>
			);
		});
	} else {
		absValuesContent = null;
	}

	return (
		<>
			<div className="RowLabel">{props.label}</div>
			<SvgComponent
				isActive={props.relOptionValue === SCRelValue.EXACT}
				onClick={() => handleRelValueClick(SCRelValue.EXACT)}
			>
				{props.exactSvg}
			</SvgComponent>
			<SvgComponent
				isActive={props.relOptionValue === SCRelValue.APPROX}
				onClick={() => handleRelValueClick(SCRelValue.APPROX)}
			>
				{props.approxSvg}
			</SvgComponent>
			<SvgComponent
				isActive={props.relOptionValue === SCRelValue.SIGNIFICANT}
				onClick={() => handleRelValueClick(SCRelValue.SIGNIFICANT)}
			>
				{props.signifSvg}
			</SvgComponent>
			<div className="AbsValues">
				{absValuesContent}
				{absValuesContent && props.onSettingsClick &&
					<button className="button AbsValueSettingButton" onClick={() => props.onSettingsClick!()} />}
			</div>
		</>
	);
};

export default ConstraintsRow;
