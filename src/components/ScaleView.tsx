import { ChangeEvent } from 'react';
import './ScaleView.scss';
import useComponentVisible from '../utils/useComponentVisible';
import { useAppDispatch, useAppSelector } from '../hooks';
import { setLeft, setRight } from '../store/ScaleSlice';

const getScaleObject = (left: number, right: number) => ({ left, right });

const scales = [
	getScaleObject(1, 2), getScaleObject(1, 2.5), getScaleObject(1, 4), getScaleObject(1, 5),
	getScaleObject(1, 10), getScaleObject(1, 15), getScaleObject(1, 20), getScaleObject(1, 25),
	getScaleObject(1, 40), getScaleObject(1, 50), getScaleObject(1, 75), getScaleObject(1, 100),
	getScaleObject(1, 200), getScaleObject(1, 400), getScaleObject(1, 500), getScaleObject(1, 800),
	getScaleObject(1, 1000), getScaleObject(2, 1), getScaleObject(2.5, 1), getScaleObject(4, 1),
	getScaleObject(5, 1), getScaleObject(10, 1), getScaleObject(20, 1), getScaleObject(40, 1),
	getScaleObject(50, 1), getScaleObject(100, 1)
]

const ScaleView = () => {

	const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

	const { leftPart, rightPart } = useAppSelector(state => state.scale);

	const dispatch = useAppDispatch();

	const getScaleString = (left: number, right: number): string => {
		return `${left.toString()}:${right.toString()}`;
	}

	const handleClick = () => {
		setIsComponentVisible(!isComponentVisible);
	}

	const handleChange = (e: ChangeEvent<HTMLInputElement>, cb: Function) => {
		e.preventDefault();
		if (e.target.value) {
			dispatch(cb(e.target.value));
		}
	}



	return (
		<div className="ScaleViewWrapper" ref={ref}>
			{ isComponentVisible &&
				<div className="OpenedContainer">
					<div className="OpenedContainerInput">
						<input name="left_part" type="number" min="1" value={leftPart} onChange={(e) => handleChange(e, setLeft)} />
						<span>:</span>
						<input name="right_part" type="number" min="1" value={rightPart} onChange={(e) => handleChange(e, setRight)} />
					</div>
					<div className="Scrollable">
						{
							scales.map((scale, i) => {
								return (
									<div key={i} className="OpenedContainerEntry">
										{getScaleString(scale.left, scale.right)}
									</div>
								);
							})
						}
					</div>
				</div>
			}
			<div className="ScaleViewContainer" onClick={handleClick}>
				<span className="ScaleLabel">Масштаб:</span>
				<span className="CurrentScale">
					{getScaleString(leftPart, rightPart)}
				</span>
			</div>
		</div>
	);
};

export default ScaleView;
