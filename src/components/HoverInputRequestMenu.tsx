import './HoverInputRequestMenu.scss';
import './ToolInputBar.scss';
import HoverMenu from './HoverMenu';
import { useAppSelector, useAppDispatch } from '../hooks';
import { setInputValue } from '../store/ToolSlice';

export interface HoverInputRequestMenu {
	x: number,
	y: number,
	onApply: Function,
}

const HoverToolbar = (props: HoverInputRequestMenu) => {

	const dispatch = useAppDispatch();

	const inputFields = useAppSelector(state => state.tool.inputFields);

	return (
		<HoverMenu x={props.x} y={props.y}>
			<div className="HoverInputRequestMenu">
				{
					inputFields.map((field, index) => {
						return (
							<label>
								{field.label}:
								<input
									type="number"
									key={index}
									name={field.fieldName.toString()}
									defaultValue={field.defaultValue}
									value={field.value}
									onChange={(e) => dispatch(setInputValue({
										newValue: Number.parseInt(e.target.value),
										fieldName: field.fieldName,
									}))}
								/>
							</label>
						);
					})
				}
				<button className="ToolInputBar--Button" onClick={(e) => props.onApply()}>ОК</button>
			</div>
		</HoverMenu>
	);
}

export default HoverToolbar;
