import DrawingContour from './dto/DrawingContour';
import HitOptions from './utils/HitOptions';

export default class AppDatabase {
	static __nextId: number = 0;
	private drawingContours: Array<DrawingContour>;

	constructor() {
		this.drawingContours = [];
	}

	removeContour(id: number): void {
		const result = this.drawingContours.find(contour => contour.id === id);
		if (result) {
			result.destroy();
			this.drawingContours.splice(id, 1);
		}
	}

	add(path: paper.Path | null = null): DrawingContour {
		const newContour = new DrawingContour(AppDatabase.__nextId++, path);
		this.drawingContours.push(newContour);
		return newContour;
	}

	getContours() {
		return this.drawingContours;
	}

	getContour(id: number): DrawingContour | null {
		const result = this.drawingContours.find(contour => contour.id === id);
		return (result) ? result : null;
	}

	hitTestContours(point: paper.Point, hitOptions: Object): [paper.HitResult | null, DrawingContour | null] {
		let hitResult: paper.HitResult | null = null;
		let hittedContour: DrawingContour | null = null;

		for (let i = 0; i < this.drawingContours.length; i++) {
			const testingPath = this.drawingContours[i].path;
			hitResult = testingPath.hitTest(point, hitOptions);
			if (hitResult) {
				hittedContour = this.drawingContours[i];
				break;
			}
		}
		
		return [hitResult, hittedContour];
	}
}
