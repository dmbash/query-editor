import ISpecialConstraint, { ISpecialConstraintMarker, SCRelValue } from './ISpecialConstraint';

export default class SCShape implements ISpecialConstraint {
	limit: number = -1;
	relValue?: SCRelValue;
	units?: string | undefined;
	value: number = -1;
	marker: ISpecialConstraintMarker = {
		type: 'Shape',
		draw: () => {
			console.log('Shape Marker Draw');
		}
	};

	constructor(relValue?: SCRelValue) {
		this.relValue = relValue;
	}
}
