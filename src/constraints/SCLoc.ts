import ISpecialConstraint, { ISpecialConstraintMarker, SCRelValue } from './ISpecialConstraint';
import DrawingContour from '../dto/DrawingContour';
import { drawMarker } from './GLoc';

export default class SCLoc implements ISpecialConstraint {
	marker: ISpecialConstraintMarker = {
		type: 'Loc',
		draw: (contour: DrawingContour) => {
			const ret = drawMarker(contour)
			this.marker.clear = ret.clear;
		},
	};
	relValue?: SCRelValue;
	value: number[];
	limit: number[];
	units?: string;

	constructor(value: number[], limit: number[], relValue?: SCRelValue) {
		this.value = value;
		this.limit = limit;
		this.relValue = relValue;
	}
}
