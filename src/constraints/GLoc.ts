import Paper from 'paper';
import CanvasTextInput from './CanvasInputText';
import DrawableInput from '../utils/DrawableInput';
import BorderBox from '../utils/BorderBox';
import DrawingContour from '../dto/DrawingContour';
import { BoxSide } from '../utils/BorderBox';
import PaperColors from '../utils/PaperColors';

const getAngleLabelPosition = (contourPoint: paper.Point, radius: number) => {
	const __contourPoint = contourPoint.clone();
	const __angleInputVector = __contourPoint.subtract(Paper.view.center);
	const angleInputPosition = __contourPoint.subtract(__angleInputVector.normalize(radius / 2));
	
	return {
		point: angleInputPosition,
		rotationAngle: __angleInputVector.angle - 180,
	}
}

const drawSettings = (contour: DrawingContour) => {

	const contourTopCenter = contour.path.bounds.topCenter.clone();
	contourTopCenter.y -= 20;

	const loc = contour.specialConstraints.loc;

	const initAngleLimit = (loc && loc.limit[0]) ? loc.limit[0].toString() : '0';
	const initLengthLimit = (loc && loc.limit[1]) ? loc.limit[1].toString() : '0';

	const centroid = new Paper.Path.Circle(Paper.view.center, 5);
	centroid.fillColor = PaperColors.Red;

	const contourCenter = new Paper.Path.Circle(contour.path.bounds.center, 3);
	contourCenter.fillColor = PaperColors.Red;

	const defVector = new Paper.Path.Line(Paper.view.center, contour.path.bounds.center);
	defVector.strokeColor = PaperColors.Yellow;
	defVector.strokeWidth = 3;

	const defCircleRadius = contour.path.bounds.center.getDistance(Paper.view.center);
	const defCircle = new Paper.Path.Circle(Paper.view.center, defCircleRadius);
	defCircle.strokeColor = PaperColors.Yellow;
	defCircle.strokeWidth = 3;
	defCircle.dashArray = [10, 4];

	let minLengthCircle : paper.Path;
	let minLengthLabel : paper.PointText;
	let maxLengthCircle : paper.Path;
	let maxLengthLabel: paper.PointText;

	const onLengthChange = (newValue: string) => {
		if (minLengthCircle) {
			minLengthCircle.remove();
		}
		if (minLengthLabel) {
			minLengthLabel.remove();
		}
		if (maxLengthCircle) {
			maxLengthCircle.remove();
		}
		if (maxLengthLabel) {
			maxLengthLabel.remove();
		}
		const length = contour.getLocation(Paper.view.center).length;
		const minLength = length - Number.parseInt(newValue);
		const maxLength = length + Number.parseInt(newValue);

		const makeCircle = (point: paper.Point, radius: number) => {
			const circle = new Paper.Path.Circle(point, radius);
			circle.strokeColor = PaperColors.LightBlue;
			circle.strokeColor.alpha = 0.5;
			circle.strokeWidth = 3;
			circle.dashArray = [10, 4];
			return circle;
		}

		const makeLabel = (point: paper.Point, content: string) => {
			const label = new Paper.PointText(point);
			label.content = content;
			label.fillColor = PaperColors.LightBlue;
			label.fontSize = 18;
			label.fontWeight = 'bold';
			return label;
		}


		minLengthCircle = makeCircle(Paper.view.center, minLength);
		const minLengthLabelPosition = contour.path.bounds.center.clone();
		minLengthLabelPosition.y -= Number.parseInt(newValue);
		minLengthLabelPosition.x += Number.parseInt(newValue);
		minLengthLabel = makeLabel(minLengthLabelPosition, minLength.toString())
		
		maxLengthCircle = makeCircle(Paper.view.center, maxLength);
		const maxLengthLabelPosition = contour.path.bounds.center.clone();
		maxLengthLabelPosition.y += Number.parseInt(newValue);
		maxLengthLabelPosition.x -= Number.parseInt(newValue);
		maxLengthLabel = makeLabel(maxLengthLabelPosition, maxLength.toString());
	}

	const lengthInputPosition = contour.path.bounds.center.clone();
	lengthInputPosition.x -= 40;
	lengthInputPosition.y -= 20;

	const lengthInput = DrawableInput(contour.getLocation(Paper.view.center).length.toString(), initLengthLimit, lengthInputPosition, PaperColors.Yellow, onLengthChange);
	
	let minAngleVector : paper.Path;
	let minAngleLabel: paper.PointText;
	let maxAngleVector: paper.Path;
	let maxAngleLabel: paper.PointText;
		
	const onAngleChange = (newValue: string) => {
		if (minAngleLabel) {
			minAngleLabel.remove();
		}
		if (maxAngleLabel) {
			maxAngleLabel.remove();
		}
		if (minAngleVector) {
			minAngleVector.remove();
		}
		if (maxAngleVector) {
			maxAngleVector.remove();
		}
		
		const angle = contour.getLocation(Paper.view.center).angle;

		minAngleVector = defVector.clone();
		minAngleVector.rotate(-Number.parseInt(newValue), Paper.view.center);
		minAngleVector.strokeColor = PaperColors.LightBlue;
		minAngleVector.strokeColor.alpha = 0.5;
		const minAngle = angle - Number.parseInt(newValue);
		const maxAngle = angle + Number.parseInt(newValue);

		const minAnglePosition = getAngleLabelPosition(minAngleVector.lastSegment.point, defCircleRadius);

		minAngleLabel = new Paper.PointText(minAnglePosition.point);
		minAngleLabel.content = minAngle.toString();
		minAngleLabel.rotate(minAnglePosition.rotationAngle);
		minAngleLabel.fillColor = PaperColors.LightBlue;
		minAngleLabel.fillColor.alpha = 0.5;
		minAngleLabel.fontSize = 18;
		minAngleLabel.fontWeight = 'bold';

		maxAngleVector = defVector.clone();
		maxAngleVector.rotate(Number.parseInt(newValue), Paper.view.center);
		maxAngleVector.strokeColor = PaperColors.LightBlue;
		maxAngleVector.strokeColor.alpha = 0.5;

		const maxAnglePosition = getAngleLabelPosition(maxAngleVector.lastSegment.point, defCircleRadius);

		maxAngleLabel = new Paper.PointText(maxAnglePosition.point);
		maxAngleLabel.content = maxAngle.toString();
		maxAngleLabel.rotate(maxAnglePosition.rotationAngle);
		maxAngleLabel.fillColor = PaperColors.LightBlue;
		maxAngleLabel.fillColor.alpha = 0.5;
		maxAngleLabel.fontSize = 18;
		minAngleLabel.fontWeight = 'bold';
	}

	const angleInputPosition = getAngleLabelPosition(contour.path.bounds.center, defCircleRadius);

	const angleInput = DrawableInput(contour.getLocation(Paper.view.center).angle.toString(), initAngleLimit, angleInputPosition.point, PaperColors.Yellow, onAngleChange);

	angleInput.rotate(angleInputPosition.rotationAngle);
	
	contourCenter.bringToFront();

	return {
		angleLimit: () => {
			return (angleInput.children[1] as CanvasTextInput).content;
		},
		lengthLimit: () => {
			return (lengthInput.children[1] as CanvasTextInput).content;
		},
		clear: () => {
			centroid.remove();
			defVector.remove();
			defCircle.remove();
			contourCenter.remove();
			lengthInput.remove();
			angleInput.remove();
			if (minAngleVector) {
				minAngleVector.remove();
			}
			if (minAngleLabel) {
				minAngleLabel.remove();
			}
			if (maxAngleVector) {
				maxAngleVector.remove();
			}
			if (maxAngleLabel) {
				maxAngleLabel.remove();
			}
			if (minLengthCircle) {
				minLengthCircle.remove();
			}
			if (maxLengthCircle) {
				maxLengthCircle.remove();
			}
			if (minLengthLabel) {
				minLengthLabel.remove();
			}
			if (maxLengthLabel) {
				maxLengthLabel.remove();
			}
		}
	}
}

const drawMarker = (contour: DrawingContour) => {
	const loc = contour.getLocation(Paper.view.center);
	const angleValue = loc.angle.toString() + '±' + (contour.specialConstraints.loc!.limit as number[])[0].toString() + '°';
	const lengthValue = loc.length.toString() + '±' + (contour.specialConstraints.loc!.limit as number[])[1].toString() + 'мм';
	const values = [angleValue, lengthValue];
	const group = BorderBox(contour.path, BoxSide.LEFT, 'М', values);
	return {
		clear: () => {
			group.remove();
		}
	}
}

export { drawSettings, drawMarker };
