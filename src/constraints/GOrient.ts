import DrawingContour from '../dto/DrawingContour';
import DrawableInput from '../utils/DrawableInput';
import CanvasTextInput from './CanvasInputText';
import PaperColors from '../utils/PaperColors';
import BorderBox, { BoxSide } from '../utils/BorderBox';

const drawSettings = (contour: DrawingContour) => {
	const contourTopCenter = contour.path.bounds.topCenter.clone();
	contourTopCenter.y -= 20;

	const initLimit = (contour.specialConstraints.orient && contour.specialConstraints.orient.limit) ? contour.specialConstraints.orient.limit.toString() : '0';

	let minLimitContour: paper.Path;
	let maxLimitContour: paper.Path;

	const makeContourClone = (prevContour: paper.Path, newOrient: number) => {
		const newContour = prevContour.clone();
		newContour.selected = false;
		newContour.strokeColor = PaperColors.LightBlue;
		newContour.strokeColor.alpha = 0.5;
		newContour.rotate(newOrient);
		return newContour;
	}

	const onNewValue = (newValue: string) => {
		if (minLimitContour) {
			minLimitContour.remove();
		}
		if (maxLimitContour) {
			maxLimitContour.remove();
		}
		const orient = contour!.getOrientation();

		minLimitContour = makeContourClone(contour.path, orient - Number.parseInt(newValue));
		maxLimitContour = makeContourClone(contour.path, orient + Number.parseInt(newValue));
	}

	const settingsInput = DrawableInput(contour.getOrientation().toString(), initLimit, contourTopCenter, PaperColors.Yellow, onNewValue);

	return {
		orientLimit: () => (settingsInput.children[1] as CanvasTextInput).content,
		clear: () => {
			settingsInput.remove();
			minLimitContour.remove();
			maxLimitContour.remove();
		}
	}
}

const drawMarker = (contour: DrawingContour) => {
	const value = [contour.getOrientation() + '±' + contour.specialConstraints.orient!.limit + '°'];
	const group = BorderBox(contour.path, BoxSide.BOTTOM, 'О', value);

	return {
		clear: () => {
			group.remove();
		}
	}
}

export { drawSettings, drawMarker };
