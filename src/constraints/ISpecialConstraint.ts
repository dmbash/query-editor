import DrawingContour from "../dto/DrawingContour";

export interface ISpecialConstraintMarker {
	type: string,
	draw(contour: DrawingContour): any;
	clear?: Function,
}

export enum SCRelValue {
	EXACT = 1,
	APPROX = 2,
	SIGNIFICANT = 3,
}

export default interface ISpecialConstraint {
	marker: ISpecialConstraintMarker,
	relValue?: SCRelValue,
	value: number | number[],
	limit: number | number[],
	units?: string,
}
