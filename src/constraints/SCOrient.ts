import DrawingContour from '../dto/DrawingContour';
import { drawMarker } from './GOrient';
import ISpecialConstraint, { ISpecialConstraintMarker, SCRelValue } from './ISpecialConstraint';

export default class SCOrient implements ISpecialConstraint {
	marker: ISpecialConstraintMarker = {
		type: 'Orient',
		draw: (contour: DrawingContour) => {
			const ret = drawMarker(contour);
			this.marker.clear = ret.clear;
		},
	};
	relValue?: SCRelValue | undefined;
	value: number | number[];
	limit: number | number[];
	units?: string | undefined;

	constructor(value: number, limit: number, relValue?: SCRelValue) {
		this.value = value;
		this.limit = limit;
		this.relValue = relValue;
	}
}
