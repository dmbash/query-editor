import Arrow from '../utils/Arrow';
import CanvasTextInput from '../constraints/CanvasInputText';
import DrawableInput from '../utils/DrawableInput';
import DrawingContour from '../dto/DrawingContour';
import Paper from 'paper';
import PaperColors from '../utils/PaperColors';

type BarPoints = {
	from: paper.Point,
	dest: paper.Point,
}

const ARROW_BAR_MARGIN = 30;

const BAR_COLOR = PaperColors.Black.clone();
BAR_COLOR.alpha = 0.8;
const BAR_WIDTH = 0.8;

// FIXME: only top and left measurement bars support for now
const createBarPoints = (from: paper.Point, vert: boolean) : BarPoints => {
	const fromClone = from.clone();
	const dest = fromClone.clone();
	if (vert) {
		dest.y -= ARROW_BAR_MARGIN;
	} else {
		dest.x -= ARROW_BAR_MARGIN;
	}
	return {
		from: fromClone,
		dest: dest,
	}
}

const makeBar = (barPoints: BarPoints) => {
	const bar = new Paper.Path.Line(barPoints.from, barPoints.dest);
	bar.strokeWidth = BAR_WIDTH;
	bar.strokeColor = BAR_COLOR;
	return bar;
}

const makeLabel = (value: number, initialLimit: string, placementPoint: paper.Point, vert: boolean = false, color: paper.Color) => {
	const groupText = DrawableInput(value.toString(), initialLimit, placementPoint, color);
	
	if (vert) {
		groupText.rotate(270, placementPoint);
	}

	return groupText;
}

const draw = (contour: DrawingContour, inSettings: boolean = true) => {
	const color = (inSettings) ? PaperColors.Yellow : PaperColors.LightBlue;
	const { path } = contour;
	const { bounds } = path;
	const currentWidth = contour.getWidth();

	const currentHeight = contour.getHeight();

	const topLeftVertPair = createBarPoints(bounds.topLeft, true);
	const topRightVertPair = createBarPoints(bounds.topRight, true);
	const topLeftHorPair = createBarPoints(bounds.topLeft, false);
	const bottomLeftHorPair = createBarPoints(bounds.bottomLeft, false);

	const topLeftVertBar = makeBar(topLeftVertPair);
	const topRightVertBar = makeBar(topRightVertPair);
	const topLeftHorBar = makeBar(topLeftHorPair);
	const bottomHorLeftBar = makeBar(bottomLeftHorPair);

	const topArrow = Arrow(topLeftVertPair.dest, topRightVertPair.dest, true);
	topArrow.fillColor = color;
	topArrow.strokeColor = color;
	const leftArrow = Arrow(topLeftHorPair.dest, bottomLeftHorPair.dest, true);
	leftArrow.fillColor = color;
	leftArrow.strokeColor = color;

	const topArrowCenterPoint = topArrow.bounds.center.clone();
	topArrowCenterPoint.y -= ARROW_BAR_MARGIN / 2;
	const leftArrowCenterPoint = leftArrow.bounds.center.clone();
	leftArrowCenterPoint.x -= ARROW_BAR_MARGIN / 2;

	const initLimitWidth = (contour.specialConstraints.size && contour.specialConstraints.size.limit[0]) ? contour.specialConstraints.size.limit[0].toString() : '0';
	const initLimitHeight = (contour.specialConstraints.size && contour.specialConstraints.size.limit[1]) ? contour.specialConstraints.size.limit[1].toString() : '0';

	const topLabel = makeLabel(currentWidth, initLimitWidth, topArrowCenterPoint, false, color);
	const leftLabel = makeLabel(currentHeight, initLimitHeight, leftArrowCenterPoint, true, color);

	const arrowsGroup = new Paper.Group(
		[
			topArrow,
			leftArrow,
			topLeftVertBar,
			topRightVertBar,
			topLeftHorBar,
			bottomHorLeftBar,
			topLabel,
			leftLabel,
		]
	);

	return {
		arrowsGroup: arrowsGroup,
		widthContent: () => (topLabel.children[1] as CanvasTextInput).content,
		heightContent: () => (leftLabel.children[1] as CanvasTextInput).content,
		clear: () => arrowsGroup.remove(),
	}
}

export default draw;
