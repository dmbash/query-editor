import Paper from 'paper';

export default class CanvasInputText extends Paper.PointText {

	oldGlobalMouseDownHandler: Function | null = null;

	defaultFontSize: string | number;
	defaultFontWeight: string | number;
	newValueCb?: Function;

	constructor(point: paper.Point, newValueCb?: Function) {
		super(point);
		this.defaultFontSize = this.fontSize;
		this.defaultFontWeight = this.fontWeight;
		this.newValueCb = newValueCb;
	}

	onMouseDown = (e: paper.MouseEvent) => {
		this.defaultFontSize = this.fontSize;
		this.defaultFontWeight = this.fontWeight;

		this.fontSize = 20;
		this.fontWeight = 700;

		this.oldGlobalMouseDownHandler = Paper.view.onMouseDown;

		Paper.view.onMouseDown = (e: paper.MouseEvent) => {
			const hitResult = this.hitTest(e.point);
			
			if (!hitResult) {
				this.fontSize = this.defaultFontSize;
				this.fontWeight = this.defaultFontWeight;
				Paper.view.onMouseDown = this.oldGlobalMouseDownHandler;
				this.oldGlobalMouseDownHandler = null;
			}
		}

		document.onkeydown = (e: KeyboardEvent) => {
			if (e.which && e.which > 47 && e.which < 57) {
				// digit input
				const strInput = String.fromCharCode(e.which);
				if (this.content === '0') {
					this.content = strInput;
				} else {
					this.content = this.content.concat(strInput);
				}
				// calback with new value
				if (this.newValueCb) {
					this.newValueCb(this.content);
				}
			} else if (e.which === 46 || e.which === 8) {
				// delete
				if (this.content.length === 1) {
					this.content = '0';
				} else {
					this.content = this.content.slice(0, this.content.length - 1);
				}
				if (this.newValueCb) {
					this.newValueCb(this.content);
				}
				// callback with new value
			}
		};
	}
}
