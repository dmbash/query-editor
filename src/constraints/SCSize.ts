import ISpecialConstraint, { ISpecialConstraintMarker, SCRelValue } from './ISpecialConstraint';
import GSizeDraw from './GSize';
import DrawingContour from '../dto/DrawingContour';

export default class SCSize implements ISpecialConstraint {
	marker: ISpecialConstraintMarker = {
		type: 'Size',
		draw: (contour: DrawingContour) => {
			const ret = GSizeDraw(contour, false);
			this.marker.clear = ret.clear;
		},
	};
	relValue?: SCRelValue;
	value: number[];
	limit: number[];
	units?: string;

	constructor(value: number[], limit: number[], relValue?: SCRelValue, units?: string ) {
		this.value = value;
		this.limit = limit;
		this.relValue = relValue;
		this.units = units;
	}
}
