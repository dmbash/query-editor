import Paper from 'paper';
import Arrow from '../utils/Arrow';
import BorderBox, { BoxSide } from '../utils/BorderBox';
import DrawingContour from '../dto/DrawingContour';
import PaperColors from '../utils/PaperColors';
import DrawableInput from '../utils/DrawableInput';
import type CanvasTextInput from './CanvasInputText';

const drawSettings = (contour: DrawingContour) => {
	const contourTopCenter = contour.path.bounds.topCenter.clone();
	contourTopCenter.y -= 20;

	const initLimit = (contour.specialConstraints.aspRatio && contour.specialConstraints.aspRatio.limit) ? contour.specialConstraints.aspRatio.limit.toString() : '0';

	let minLimitContour : paper.Path;
	let maxLimitContour : paper.Path;

	const makeContourClone = (prevContour: paper.Path, newAsp: number) => {
		const newContour = prevContour.clone();
		newContour.selected = false;
		newContour.strokeColor = PaperColors.LightBlue;
		newContour.strokeColor.alpha = 0.5;
		newContour.bounds.width = newContour.bounds.height * newAsp;
		newContour.bounds.height = newContour.bounds.width / newAsp;
		return newContour;
	}

	const onNewValue = (newValue: string) => {
		if (minLimitContour) {
			minLimitContour.remove();
		}
		if (maxLimitContour) {
			maxLimitContour.remove();
		}
		const aspRatio = contour!.getAspRatio();
		const limitDiff = Math.round(((aspRatio * Number.parseFloat(newValue)) / 100) * 10) / 10;
		const minAspRatio = aspRatio - limitDiff;
		const maxAspRatio = aspRatio + limitDiff;
		
		minLimitContour = makeContourClone(contour.path, minAspRatio);
		maxLimitContour = makeContourClone(contour.path, maxAspRatio);		
	}

	const settingsInput = DrawableInput(contour.getAspRatio().toString(), initLimit, contourTopCenter, PaperColors.Yellow, onNewValue);

	return {
		aspLimit: () => (settingsInput.children[1] as CanvasTextInput).content,
		clear: () => {
			settingsInput.remove();
			minLimitContour.remove();
			maxLimitContour.remove();
		}
	}
}

const drawMarker = (contour: DrawingContour) => {
	// make black box with label 'OC'
	const value = [contour.getAspRatio() + '±' + contour.specialConstraints.aspRatio!.limit + '%'];
	const group = BorderBox(contour.path, BoxSide.RIGHT, 'ОС', value);

	return {
		clear: () => {
			group.remove();
		}
	}
}

export { drawSettings, drawMarker }
