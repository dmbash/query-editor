import ISpecialConstraint, { ISpecialConstraintMarker, SCRelValue } from './ISpecialConstraint';
import { drawMarker as DrawAspRatioMarker } from './GAspRatio';
import DrawingContour from '../dto/DrawingContour';

export default class SCAspRatio implements ISpecialConstraint {
	marker: ISpecialConstraintMarker = {
		type: 'AspRatio',
		draw: (contour: DrawingContour) => {
			const ret = DrawAspRatioMarker(contour);
			this.marker.clear = ret.clear;
		},
	};
	relValue?: SCRelValue;
	value: number;
	limit: number;
	units?: string;

	constructor(value: number, limit: number, relValue?: SCRelValue) {
		this.value = value;
		this.limit = limit;
		this.relValue = relValue;
	}
	
}
