import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface ToolState {
	name: String | null,
	localizedName: String | null,
	needInputBar: boolean,
	inputFields: Array<ToolInputField>,
	hasSelectedContour: boolean,
}

export type ToolInputField = {
	label: String,
	fieldName: String,
	defaultValue: number,
	value?: number,
};

const initialState: ToolState = {
	name: null,
	localizedName: null,
	needInputBar: false,
	inputFields: [],
	hasSelectedContour: false,
}

export const toolSlice = createSlice({
	name: 'tool',
	initialState,
	reducers: {
		select: (state, action: PayloadAction<{name: String | null, localizedName: String | null, needInputBar: boolean}>) => {
			const { name, localizedName, needInputBar } = action.payload;
			state.name = name;
			state.localizedName = localizedName;
			state.needInputBar = needInputBar;
		},
		reset: (state) => {
			state.name = null;
			state.localizedName = null;
			state.needInputBar = false;
			state.inputFields = [];
		},
		setToolInputFields: (state, action: PayloadAction<Array<ToolInputField>>) => {
			state.inputFields = action.payload;
		},
		setHasSelectedContour: (state, action: PayloadAction<boolean>) => {
			state.hasSelectedContour = action.payload;
		},
		setInputValue: (state, action: PayloadAction<{newValue: number, fieldName: String}>) => {
			const { newValue, fieldName } = action.payload;
			const field = state.inputFields.find((field) => field.fieldName === fieldName);
			if (field) {
				field.value = newValue;
			}
		},
	}
});

export const { select, reset, setToolInputFields, setInputValue, setHasSelectedContour } = toolSlice.actions;

export default toolSlice.reducer;

