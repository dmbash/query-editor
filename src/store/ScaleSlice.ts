import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface ScaleState {
	leftPart: number,
	rightPart: number,
}

const initialState : ScaleState = {
	leftPart: 1,
	rightPart: 1,
}

export const scaleSlice = createSlice({
	name: 'scale',
	initialState,
	reducers: {
		setLeft: (state, action: PayloadAction<number>) => {
			state.leftPart = action.payload;
		},
		setRight: (state, action: PayloadAction<number>) => {
			state.rightPart = action.payload;
		},
	}
});

export const { setLeft, setRight } = scaleSlice.actions;

export default scaleSlice.reducer;

