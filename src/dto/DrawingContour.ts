import Paper from 'paper';
import PaperColors from '../utils/PaperColors';
import SCShape from '../constraints/SCShape';
import SCSize from '../constraints/SCSize';
import SCAspRatio from '../constraints/SCAspRatio';
import SCOrient from '../constraints/SCOrient';
import SCLoc from '../constraints/SCLoc';
import { isJsxClosingElement } from 'typescript';

export enum ContourRole {
	DEFAULT = 1,
	BASE = 2,
	REQUIRED = 3,
}

type SpecialConstraints = {
	shape: SCShape | null,
	size: SCSize | null,
	aspRatio: SCAspRatio | null,
	orient: SCOrient | null,
	loc: SCLoc | null,
	contourRole: ContourRole,
}

const initialConstraints : SpecialConstraints = {
	shape: null,
	size: null,
	aspRatio: null,
	orient: null,
	loc: null,
	contourRole: ContourRole.DEFAULT,
};

export default class DrawingContour {
	id: number;
	path: paper.Path;
	specialConstraints: SpecialConstraints;

	constructor(id: number, path: paper.Path | null = null, specialConstraints?: SpecialConstraints) {
		this.id = id;
		if (!path) {			
			this.path = new Paper.Path({
				strokeWidth: 3,
				strokeColor: PaperColors.DeepBlue,
			});
		} else {
			this.path = path;
			this.path.strokeWidth = 3;
			this.path.strokeColor = PaperColors.DeepBlue;
		}
		if (specialConstraints) {
			this.specialConstraints = specialConstraints;
		} else {
			this.specialConstraints = { ...initialConstraints };
		}
	}

	static serialize(contour: DrawingContour) {
		const serializedPath = contour.path.exportJSON();
		return JSON.stringify({id: contour.id, path: serializedPath, specialConstraints: contour.specialConstraints});
	}

	static deserialize(serializedContour: string): DrawingContour {
		const contour = JSON.parse(serializedContour);
		const path = (Paper.project.importJSON(contour.path)) as paper.Path;
		return new DrawingContour(contour.id, path, contour.specialConstraints);
	}

	getRolePathColor() {
		if (this.path.selected) {
			return PaperColors.Yellow;
		}
		
		switch (this.specialConstraints.contourRole) {
			case ContourRole.DEFAULT:
				return PaperColors.DeepBlue;
			case ContourRole.REQUIRED:
				return PaperColors.Black;
			case ContourRole.BASE:
				return PaperColors.Red;
		}
	}

	updateStroke() {
		this.path.strokeColor = this.getRolePathColor();
	}

	destroy() {
		this.path.remove();
		this.specialConstraints = {...initialConstraints};
	}

	// helper functions for spec. constraints parameters

	getWidth(): number {
		return Math.round(this.path.bounds.width);
	}

	getHeight(): number {
		return Math.round(this.path.bounds.height);
	}

	getAspRatio(): number {
		return Math.round((this.getWidth() / this.getHeight()) * 10) / 10;
	}

	getOrientation(): number {
		// TODO return actual orientation, but how?
		return 0;
	}

	getLocation(centroid: paper.Point): {angle: number, length: number} {
		const pathCenter = this.path.bounds.center;

		const centroidRight = centroid.clone();
		centroidRight.x = centroidRight.x + 1;

		const angle = Math.round(centroidRight.subtract(centroid).getDirectedAngle(pathCenter.subtract(centroid)) ) * -1;

		const length = Math.round(centroid.getDistance(pathCenter));
		
		return {
			angle,
			length,
		};
	}
}
