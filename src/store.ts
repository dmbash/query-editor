import { configureStore } from '@reduxjs/toolkit';
import toolReducer from './store/ToolSlice';
import scaleReducer from './store/ScaleSlice';

const store = configureStore({
	reducer: {
		tool: toolReducer,
		scale: scaleReducer,
	}
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export default store;
