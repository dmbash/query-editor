import Tool from "./Tool";
import Paper from 'paper';
import IsRightClicked from '../utils/IsRightClicked';
import PaperColors from "../utils/PaperColors";
import { ToolInputField } from "../store/ToolSlice";

export default class EllipseTool extends Tool {
	centerPoint: paper.Point | null = null;
	radius: number = 0;
	
	getName(): string {
		return "ellipse";
	}
	getLocalizedName(): string {
		return "Эллипс";
	}
	getNeedInputBar(): boolean {
		return false;
	}
	getInputFields(): ToolInputField[] {
		return [];
	}
	resetState(): void {
		this.centerPoint = null;
	}
	onMouseUp(event: paper.ToolEvent): void {

	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		if (this.centerPoint) {
			this.handler.appDb.add(this.createCirlce(this.centerPoint, this.radius));
			this.centerPoint = null;
		} else {
			this.centerPoint = event.point;
		}
	}
	onKeyUp(event: paper.KeyEvent): void {

	}
	onKeyDown(event: paper.KeyEvent): void {

	}
	onMouseMove(event: paper.ToolEvent): void {
		if (this.centerPoint) {
			this.radius = this.centerPoint.getDistance(event.point);
			const tempCircle = this.createCirlce(this.centerPoint, this.radius);
			tempCircle.removeOnMove();
		}
	}
	onMouseDrag(event: paper.ToolEvent): void {

	}

	createCirlce(center: paper.Point, radius: number) : paper.Path.Circle {
		return new Paper.Path.Circle({
			strokeColor: PaperColors.DeepBlue,
			strokeWidth: 3,
			center,
			radius
		});
	}
}
