export default interface IApplicable {
	apply(): void;
}
