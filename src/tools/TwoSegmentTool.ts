import Tool from './Tool';
import Paper from 'paper';
import HitOptions from '../utils/HitOptions';
import MarkerWithText from '../utils/MarkerWithText';
import { HasOneCommonSegment } from '../utils/PathUtils';

export default abstract class TwoSegmentTool extends Tool {
	firstSegment?: paper.CurveLocation;
	secondSegment?: paper.CurveLocation;
	firstMarker?: paper.Group;
	secondMarker?: paper.Group;
	commonSegment?: paper.Segment;

	cleanupSegments() {
		if (this.firstMarker) {
			this.firstMarker.remove();
			delete this.firstMarker;
		}

		if (this.secondMarker) {
			this.secondMarker.remove();
			delete this.secondMarker;
		}

		delete this.firstSegment;
		delete this.secondSegment;
		delete this.commonSegment;
	}

	relocateMarker(marker: paper.Group, newPosition: paper.Point, labelOffset: number = 12) {
		marker.children[0].position = newPosition;
		marker.children[1].position = newPosition.subtract(labelOffset);
	}

	selectSegments(point: paper.Point) {
		const [hitResult, _] = this.handler.appDb.hitTestContours(point, HitOptions(5));

		if (hitResult && hitResult.type === 'stroke') {
			if (!this.firstSegment) {
				this.firstSegment = hitResult.location;
				this.firstMarker = MarkerWithText(this.firstSegment.point, '1');
			} else if (!this.secondSegment) {
				const { segment1, segment2 } = this.firstSegment.curve;
				const { segment1: segment3, segment2: segment4 } = hitResult.location.curve;
				const { duplicates, common } = HasOneCommonSegment(segment1, segment2, segment3, segment4);
				if (duplicates === 1) {
					this.secondSegment = hitResult.location;
					this.secondMarker = MarkerWithText(this.secondSegment.point, '2');
					this.commonSegment = common;
				}
			}
		}
	}
}
