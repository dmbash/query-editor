import PaperColors from "../utils/PaperColors";
import Paper from 'paper';
import Tool from "./Tool";
import IsRightClicked from "../utils/IsRightClicked";
import { ToolInputField } from "../store/ToolSlice";

// this tool draws curve via points
export default class CurveTool extends Tool {

	currentSegment: paper.Segment | null = null;
	currentPath: paper.Path | null = null;
	mode: String | null = null;
	type: String | null = null;

	getName(): string {
		return "curve";
	}
	getLocalizedName(): string {
		return "Кривая";
	}
	getNeedInputBar(): boolean {
		return false;
	}
	getInputFields(): ToolInputField[] {
		return [];
	}

	resetState(): void {
		this.currentSegment = null;
		this.mode = null;
		this.type = null;
		this.currentPath = null;
	}

	getHandle(point: paper.Point) {
		const types = ['point', 'handleIn', 'handleOut'];
		if (this.currentPath) {
			for (let i = 0; i < this.currentPath.segments.length; i++) {
				for (let j = 0; j < 3; j++) {
					const type = types[j];
					const segment = this.currentPath.segments[i];
					let segmentPoint: paper.Point;
					switch (type) {
						case 'point':
							segmentPoint = segment.point;
							break;
						case 'handleIn':
							segmentPoint = segment.point.add(segment.handleIn);
							break;
						case 'handleOut':
							segmentPoint = segment.point.add(segment.handleOut);
							break;
					}
					const distance = point.getDistance(segmentPoint!);
					if (distance < 3) {
						return {
							type,
							segment,
						}
					}
				}
			}
		}
		return null;
	}

	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		if (this.currentSegment) {
			this.currentSegment.selected = false;
		}

		this.mode = this.type = this.currentSegment = null;

		if (this.currentPath == null) {
			this.currentPath = this.handler.appDb.add().path;
		}

		const result = this.getHandle(event.point);


		if (result) {
			this.currentSegment = result.segment;
			this.type = result.type;
			// this is check for if clicked to first segment
			if (this.currentPath.length > 1 && result.type === 'point' && result.segment.index === 0) {
				this.mode = 'close';
				this.currentPath.closed = true;
				this.currentPath.selected = false;
				this.currentPath = null;
			}
		}

		if (this.mode !== 'close') {
			this.mode = this.currentSegment ? 'move' : 'add';
			if (!this.currentSegment) {
				this.currentSegment = this.currentPath!.add(event.point) as paper.Segment;
			}
			this.currentSegment.selected = true;
		}
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
	}
	onMouseDrag(event: paper.ToolEvent): void {
		if (this.mode === 'move' && this.type === 'point') {
			this.currentSegment!.point = event.point;
		} else if (this.mode !== 'close') {
			let delta = event.delta.clone();
			if (this.type === 'handleOut' || this.mode === 'add') {
				delta = delta.multiply(-1);
			}
			this.currentSegment!.handleIn = this.currentSegment!.handleIn.add(delta);
			this.currentSegment!.handleOut = this.currentSegment!.handleOut.subtract(delta);
		}
	}
}
