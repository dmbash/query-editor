import Paper from 'paper';
import PaperColors from '../utils/PaperColors';
import HitOptions from "../utils/HitOptions";
import Tool from "./Tool";
import IsRightClicked from '../utils/IsRightClicked';
import { ToolInputField } from '../store/ToolSlice';

export default class PointTool extends Tool {
	getName(): string {
		return "point";
	}
	getLocalizedName(): string {
		return "Точка";
	}
	getNeedInputBar(): boolean {
		return false;
	}
	getInputFields(): ToolInputField[] {
		return [];
	}
	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		const [hitResult, contour] = this.handler.appDb.hitTestContours(event.point, HitOptions(5));

		if (hitResult && hitResult.location) {
			contour!.path.insert(hitResult.location.index + 1, hitResult.point);
		}
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
		const [hitResult, _] = this.handler.appDb.hitTestContours(event.point, HitOptions(5));

		if (hitResult && hitResult.type === 'stroke') {
			const pointMarker = new Paper.Path.Circle({center: hitResult.point, radius: 5, fillColor: PaperColors.Yellow});
			pointMarker.removeOnMove();
		}
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}

	resetState(): void {
		return;
	}

}
