import Paper from 'paper';
import ToolsHandler from '../handlers/ToolsHandler';
import type { ToolInputField } from '../store/ToolSlice';

abstract class Tool {
	abstract getName(): string;
	abstract getLocalizedName(): string;
	abstract getNeedInputBar(): boolean;
	abstract getInputFields(): Array<ToolInputField> | [];

	private __Tool: paper.Tool;

	handler: ToolsHandler;

	constructor(handler: ToolsHandler) {
		this.handler = handler;

		this.__Tool = new Paper.Tool();
		this.__Tool.onKeyDown = (event: paper.KeyEvent) => this.onKeyDown.call(this, event);
		this.__Tool.onKeyUp = (event: paper.KeyEvent) => this.onKeyUp.call(this, event);
		this.__Tool.onMouseUp = (event: paper.ToolEvent) => this.onMouseUp.call(this, event);
		this.__Tool.onMouseDown = (event: paper.ToolEvent) => this.onMouseDown.call(this, event);
		this.__Tool.onMouseMove = (event: paper.ToolEvent) => this.onMouseMove.call(this, event);
		this.__Tool.onMouseDrag = (event: paper.ToolEvent) => this.onMouseDrag.call(this, event);
	}

	abstract onMouseUp(event: paper.ToolEvent): void;
	abstract onMouseDown(event: paper.ToolEvent): void;
	abstract onKeyUp(event: paper.KeyEvent): void;
	abstract onKeyDown(event: paper.KeyEvent): void;
	abstract onMouseMove(event: paper.ToolEvent): void;
	abstract onMouseDrag(event: paper.ToolEvent): void;
	abstract resetState(): void;

	activate() {
		this.__Tool.activate();
		this.handler.currentTool = this;
		this.handler.onActivate(this);
	}

	abort() {
		this.resetState();
		this.handler.toolsList.SelectTool.activate();
	}

}

export default Tool;
