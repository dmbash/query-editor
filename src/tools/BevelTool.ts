import HitOptions from "../utils/HitOptions";
import IApplicable from './IApplicable';
import IsRightClicked from "../utils/IsRightClicked";
import Paper from 'paper';
import PaperColors from "../utils/PaperColors";
import Tool from "./Tool";
import store from '../store';
import { getRelativeCurvePoint, HasOneCommonSegment } from "../utils/PathUtils";
import MarkerWithText from "../utils/MarkerWithText";
import TwoSegmentTool from "./TwoSegmentTool";

export default class BevelTool extends TwoSegmentTool implements IApplicable {

	length_1?: number;
	length_2?: number;
	line?: paper.Path;
	__subscribeLock?: Function;

	apply(): void {
		if (this.firstMarker && this.secondMarker) {
			this.commonSegment!.path.divideAt(this.commonSegment!.path.getLocationOf(this.firstMarker.children[0].position));
			this.commonSegment!.path.divideAt(this.commonSegment!.path.getLocationOf(this.secondMarker.children[0].position));
			this.commonSegment!.remove();
			this.handler.onApplied(this);
			this.handler.currentTool!.abort();
		}
	}
	getName(): string {
		return "bevel";
	}
	getLocalizedName(): string {
		return "Фаска";
	}
	getNeedInputBar(): boolean {
		return true;
	}
	getInputFields() {
		return [
			{
				label: "Длина 1",
				fieldName: "length_1",
				defaultValue: 10,
			},
			{
				label: "Длина 2",
				fieldName: "length_2",
				defaultValue: 10,
			},
		]
	}
	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		if (!this.__subscribeLock) {
			if (!this.length_1 || !this.length_2) {
				const [length1, length2] = store.getState().tool.inputFields;
				this.length_1 = length1.defaultValue;
				this.length_2 = length2.defaultValue;
			}

			this.__subscribeLock = store.subscribe(() => {
				const [length1, length2] = store.getState().tool.inputFields;
				this.length_1 = (length1.value) ? length1.value : length1.defaultValue;
				this.length_2 = (length2.value) ? length2.value : length2.defaultValue;
				this.redraw();
			});
		}

		this.selectSegments(event.point);

		this.redraw();
	}

	redraw() {
		if (this.commonSegment) {
			if (this.firstMarker && this.secondMarker) {
				this.relocateMarker(this.firstMarker, getRelativeCurvePoint(this.commonSegment, this.firstSegment!.curve, this.length_1!));
				this.relocateMarker(this.secondMarker, getRelativeCurvePoint(this.commonSegment, this.secondSegment!.curve, this.length_2!));
				if (this.line) {
					this.line.remove();
				}
				this.line = new Paper.Path.Line(this.firstMarker.children[0].position, this.secondMarker.children[0].position);
				this.line.strokeWidth = 1.5;
				this.line.dashArray = [10, 4];
				this.line.strokeColor = PaperColors.Yellow;

				const { x, y } = this.line.lastSegment.point;

				this.handler.onInputRequest(x + 50, y);
			}
		}
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}
	resetState(): void {
		if (this.__subscribeLock) {
			this.__subscribeLock();
			delete this.__subscribeLock;
		}
		if (this.line) {
			this.line.remove();
			delete this.line;
		}

		delete this.length_1;
		delete this.length_2;

		this.cleanupSegments();
	}

}
