import Tool from './Tool';

export default class DummyTool extends Tool {
  getName(): string {
		return "dummy";
    }
  getLocalizedName(): string {
		return "";
    }
  getNeedInputBar(): boolean {
		return false;
  }
	getInputFields() {
		return [];
	}
	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}
	resetState(): void {
	}
}
