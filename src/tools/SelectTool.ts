import PaperColors from '../utils/PaperColors';
import HitOptions from '../utils/HitOptions';
import Tool from './Tool';
import { ToolInputField } from '../store/ToolSlice';

export default class SelectTool extends Tool {

	pathToCopy?: paper.Path;
		
	getName(): string {
		return "select";
	}
	getLocalizedName(): string {
		return "Выделение";
	}
	getNeedInputBar(): boolean {
		return false
	}
	getInputFields(): ToolInputField[] {
		return [];
	}
	onMouseUp(event: paper.ToolEvent): void {
		return;
	}
	resetState(): void {
		return;
	}
	
	onMouseDown(event: paper.ToolEvent): void {
		const [hitResult, contour] = this.handler.appDb.hitTestContours(event.point, HitOptions(5));

		if (contour && hitResult) {
			this.handler.selectionHandler.select(contour, hitResult.segment, hitResult.type);
		} else {
			this.handler.selectionHandler.deselectAll();
		}
	}

	onKeyUp(event: paper.KeyEvent): void {
		if (event.key === 'delete') {
			if (this.handler.selectionHandler.hasSelectedSegment()) {
				this.handler.selectionHandler.getSelectedSegment()!.remove();
				this.handler.selectionHandler.deselectSegment();
			} else if (this.handler.selectionHandler.hasSelectedContour()) {
				this.handler.appDb.removeContour(this.handler.selectionHandler.getSelectedContour()!.id);
				this.handler.selectionHandler.deselectAll();
			}
		}
	}
	
	onKeyDown(event: paper.KeyEvent): void {
		if (event.key === 'c') {
			const contour = this.handler.selectionHandler.getSelectedContour();
			if (contour) {
				this.pathToCopy = contour.path;
			}
			// copy
			return;
		}

		if (event.key === 'v') {
			// paste
			if (this.pathToCopy) {
				this.handler.appDb.add(this.pathToCopy.clone());
				delete this.pathToCopy;
			}
			return;
		}
		return;
	}
	onMouseMove(event: paper.ToolEvent): void {
		return;
	}
	onMouseDrag(event: paper.ToolEvent): void {
		const selectedType = this.handler.selectionHandler.getSelectedType();

		if (!selectedType) {
			return;
		}

		const [hitResult, _] = this.handler.appDb.hitTestContours(event.point, HitOptions(10));

		if (selectedType === 'handle-in') {
			const segment = this.handler.selectionHandler.getSelectedSegment()!;
			segment.handleIn = segment.handleIn.add(event.delta);
			return;
		} else if (selectedType === 'handle-out') {
			const segment = this.handler.selectionHandler.getSelectedSegment()!;
			segment.handleOut = segment.handleOut.add(event.delta);
			return;
		}

		if (this.handler.selectionHandler.hasSelectedSegment()) {
			const segment = this.handler.selectionHandler.getSelectedSegment()!;
			if (hitResult && hitResult.type === 'segment') {
				segment.point = hitResult.segment.point;
				return;
			}
			segment.point = segment.point.add(event.delta);
			return;
		}

		if (this.handler.selectionHandler.hasSelectedContour()) {
			const path = this.handler.selectionHandler.getSelectedContour()!.path;
			path.position = path.position.add(event.delta);
		}
	}
}
