import IsRightClicked from "../utils/IsRightClicked";
import Paper from 'paper';
import PaperColors from '../utils/PaperColors';
import Tool from "./Tool";
import { ToolInputField } from "../store/ToolSlice";

export default class RectangleTool extends Tool {

	anchorPoint: paper.Point | null = null;
	
	getName(): string {
		return "rectangle";
	}
	getLocalizedName(): string {
		return "Прямоугольник";
	}
	getNeedInputBar(): boolean {
		return false;
	}
	getInputFields(): ToolInputField[] {
		return [];
	}
	resetState(): void {
		this.anchorPoint = null;
	}

	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		if (this.anchorPoint) {
			this.handler.appDb.add(this.createRectangle(this.anchorPoint, event.point));
			this.anchorPoint = null;
		} else {
			this.anchorPoint = new Paper.Point(event.point);
		}
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
		if (this.anchorPoint) {
			const temporaryRect = this.createRectangle(this.anchorPoint, event.point);
			temporaryRect.removeOnMove();
		}
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}

	createRectangle(anchorPoint: paper.Point, endPoint: paper.Point) {
		const rect = new Paper.Path.Rectangle(anchorPoint, endPoint);
		rect.strokeWidth = 3;
		rect.strokeColor = PaperColors.DeepBlue;
		return rect;
	}
}

