import IApplicable from './IApplicable';
import IsRightClicked from "../utils/IsRightClicked";
import store from '../store';
import Paper from 'paper';
import { ToolInputField } from "../store/ToolSlice";
import { getRelativeCurvePoint } from '../utils/PathUtils';
import TwoSegmentTool from './TwoSegmentTool';
import PaperColors from '../utils/PaperColors';

export default class RoundTool extends TwoSegmentTool implements IApplicable {
	radius?: number;
	curve?: paper.Path;
	controlPoint?: paper.Point;
	__subscribeLock?: Function;

	getInputFields(): ToolInputField[] {
		return [
			{
				label: "Радиус",
				fieldName: "radius",
				defaultValue: 10,
			}
		];
	}

	apply(): void {
		if (this.commonSegment && this.firstMarker && this.secondMarker) {
			const newSegment1 = this.commonSegment!.path.divideAt(this.commonSegment.path.getLocationOf(this.firstMarker.children[0].position));
			const newSegment2 = this.commonSegment.path.divideAt(this.commonSegment.path.getLocationOf(this.secondMarker.children[0].position));
			// FIXME detect handles to flip it
			newSegment1.handleIn = this.curve!.firstCurve.handle1;
			newSegment1.handleOut = this.curve!.firstCurve.handle2;
			newSegment2.handleIn = this.curve!.firstCurve.handle1;
			newSegment2.handleOut = this.curve!.firstCurve.handle2;
			this.commonSegment.remove();
			this.handler.onApplied(this);
			this.handler.currentTool!.abort();
		}

	}
	getName(): string {
		return "round";
	}
	getLocalizedName(): string {
		return "Скругление";
	}
	getNeedInputBar(): boolean {
		return true;
	}
	onMouseUp(event: paper.ToolEvent): void {
	}
	onMouseDown(event: paper.ToolEvent): void {
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			this.handler.onApplied(this);
			return;
		}

		if (!this.__subscribeLock) {
			if (!this.radius) {
				this.radius = store.getState().tool.inputFields[0].defaultValue;
			}
			this.__subscribeLock = store.subscribe(() => {
				const [radius] = store.getState().tool.inputFields;
				this.radius = (radius.value) ? radius.value : radius.defaultValue;
				this.redraw();
			});
		}

		this.selectSegments(event.point);

		this.redraw();
	}

	redraw() {
		if (this.commonSegment && this.firstMarker && this.secondMarker) {
			this.relocateMarker(this.firstMarker, getRelativeCurvePoint(this.commonSegment, this.firstSegment!.curve, this.radius!));
			this.relocateMarker(this.secondMarker, getRelativeCurvePoint(this.commonSegment, this.secondSegment!.curve, this.radius!));
			if (this.curve) {
				this.curve.remove();
			}

			this.controlPoint = this.commonSegment.point.add((this.radius! * Math.sqrt(2))/2);

			const isInsideRect = (point: paper.Point) => {
				const testingRectangle = new Paper.Rectangle(this.firstMarker!.children[0].position, this.secondMarker!.children[0].position);
				return point.isInside(testingRectangle);
			}

			while (!isInsideRect(this.controlPoint)) {
				this.controlPoint = this.controlPoint.rotate(90, this.commonSegment.point);
			}
			
			this.curve = new Paper.Path.Arc(this.firstMarker.children[0].position, this.controlPoint,this.secondMarker.children[0].position)
			this.curve.strokeWidth = 1.5;
			this.curve.dashArray = [10, 4];
			this.curve.strokeColor = PaperColors.Yellow;

			const { x, y } = this.curve.lastSegment.point;

			this.handler.onInputRequest( x + 50, y);
		}
	}
	onKeyUp(event: paper.KeyEvent): void {
	}
	onKeyDown(event: paper.KeyEvent): void {
	}
	onMouseMove(event: paper.ToolEvent): void {
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}
	resetState(): void {
		if (this.__subscribeLock) {
			this.__subscribeLock();
			delete this.__subscribeLock;
		}

		if (this.curve) {
			this.curve.remove();
			delete this.curve;
		}

		delete this.radius;

		this.cleanupSegments();
	}

}
