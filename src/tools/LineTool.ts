import HitOptions from '../utils/HitOptions';
import Paper from 'paper';
import PaperColors from "../utils/PaperColors";
import Tool from "./Tool";
import IsRightClicked from '../utils/IsRightClicked';
import { ToolInputField } from '../store/ToolSlice';

export default class LineTool extends Tool {

	currentLine?: paper.Path;
	currentContourId: number = -1;
	onceClicked: boolean = false;
	shiftPressed: boolean = false;

	getName(): string {
		return "line";
	}
	getLocalizedName(): string {
		return "Линия";
	}
	getNeedInputBar(): boolean {
		return false;
	}
	getInputFields(): ToolInputField[] {
		return [];
	}
	resetState(): void {
		if (this.currentLine) {
			this.currentLine.removeSegment(this.currentLine.lastSegment.index);
			if (this.currentLine.segments.length < 2) {
				this.handler.appDb.removeContour(this.currentContourId);
			}
			if (this.currentLine.firstSegment.point.equals(this.currentLine.lastSegment.point)) {
				this.currentLine.closePath();
			}
		}
		this.currentContourId = -1;
		this.onceClicked = false;
		this.currentLine = undefined;
	}
	onMouseUp(event: paper.ToolEvent): void {
	}

	onMouseDown(event: paper.ToolEvent): void {
		// finish operation and activate SelectTool if right click was pressed
		if (IsRightClicked(event)) {
			this.handler.currentTool!.abort();
			return;
		}

		if (!this.currentLine) {
			const contour = this.handler.appDb.add();
			this.currentLine = contour.path;
			this.currentContourId = contour.id;
			this.onceClicked = true;
		}

		this.currentLine.add(event.point);
	}

	onKeyUp(event: paper.KeyEvent): void {
		if (event.key === "shift")
		  this.shiftPressed = false;
	}

	onKeyDown(event: paper.KeyEvent): void {
		if (event.key === 'shift')
		  this.shiftPressed = !this.shiftPressed;
	}
	onMouseMove(event: paper.ToolEvent): void {
		const [hitResult, _] = this.handler.appDb.hitTestContours(event.point, HitOptions(10));

		if (this.currentLine && hitResult && hitResult.type === 'segment') {
			this.currentLine.lastSegment.point = hitResult.segment.point;
			return;
		}

		if (this.onceClicked && this.currentLine) {
			if (this.currentLine.segments.length === 1) {
				this.currentLine.add(event.point);
			}

			if (this.currentLine.length > 0) {
				if (this.shiftPressed) {
					const vector = event.point.subtract(this.currentLine.lastSegment.previous.point);
					vector.angle = Math.round(vector.angle/45)*45;
					this.currentLine.lastSegment.point = this.currentLine.lastSegment.previous.point.add(vector);
				} else {
					this.currentLine.lastSegment.point = event.point;
				}

			}
		}
	}
	onMouseDrag(event: paper.ToolEvent): void {
	}
}
