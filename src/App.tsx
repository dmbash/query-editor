import './App.scss';
import AppDatabase from './AppDatabase';
import AppFooter from './components/AppFooter';
import AppHeader from './components/AppHeader';
import ConstraintsModal from './components/ConstraintsModal'
import DrawingHandler from './handlers/DrawingHandler';
import HoverInputRequestMenu from './components/HoverInputRequestMenu';
import Paper from 'paper';
import ReactModal from 'react-modal';
import SelectionHandler from './handlers/SelectionHandler';
import ToolsHandler from './handlers/ToolsHandler';
import { useEffect, createRef, createContext, useRef, useState } from 'react';
import FileHandler from './handlers/FileHandler';

export interface IAppContext {
	getToolsHandler: Function;
	constraintsFunctions: ConstraintsModalFunctions;
	getFileHandler: Function;
}

type ConstraintsModalFunctions = {
	onOpen: Function,
	onOk: Function,
	onCancel: Function,
};

export const AppContext = createContext<IAppContext | null>(null);

const App = () => {

	const [ hoverToolbarMenuX, setHoverToolbarMenuX ] = useState(0);
	const [ hoverToolbarMenuY, setHoverToolbarMenuY ] = useState(0);
	const [ isHoverToolbarMenuVisible, setIsHoverToolbarMenuVisible ] = useState(false);
	const [ isConstraintsModalVisible, setIsConstraintsModalVisible ] = useState(false);

	ReactModal.setAppElement('#root');

	const canvasRef = createRef<HTMLCanvasElement>();
	const appDbRef = useRef<AppDatabase>();
	const drawingHandlerRef = useRef<DrawingHandler>();
	const selectionHandlerRef = useRef<SelectionHandler>();
	const toolsHandlerRef = useRef<ToolsHandler>();
	const fileHandlerRef = useRef<FileHandler>();
	
	const onInputRequest = (x: number, y: number): void => {
		setHoverToolbarMenuX(x);
		setHoverToolbarMenuY(y);
		setIsHoverToolbarMenuVisible(true);
	}

	const onToolApply = (): void => {
		if (toolsHandlerRef.current) {
			const __toolsHandler = toolsHandlerRef.current;
			if (__toolsHandler.isApplicable(__toolsHandler.currentTool)) {
				__toolsHandler.currentTool.apply();
			}
		}
	}

	const constraintsFunctions: ConstraintsModalFunctions = {
		onOpen: () => {
			setIsConstraintsModalVisible(true);
		},
		onOk: () => {
			toolsHandlerRef.current!.activateToolByName('select');
			const selectedContour = selectionHandlerRef.current!.getSelectedContour();

			if (selectedContour!.specialConstraints.size) {
				const __size = selectedContour!.specialConstraints.size;
				if (__size.limit[0] !== 0 || __size.limit[1] !== 0) {
					selectedContour!.specialConstraints.size.marker.draw(selectedContour!);
				}
			}

			if (selectedContour!.specialConstraints.aspRatio) {
				const __aspRatio = selectedContour!.specialConstraints.aspRatio;

				if (__aspRatio.limit !== 0) {
					__aspRatio.marker.draw(selectedContour!);
				}
			}

			if (selectedContour!.specialConstraints.orient) {
				const __orient = selectedContour!.specialConstraints.orient;

				if (__orient.limit !== 0) {
					__orient.marker.draw(selectedContour!);
				}
			}

			if (selectedContour!.specialConstraints.loc) {
				const __loc = selectedContour!.specialConstraints.loc;

				if (__loc.limit[0] !== 0 || __loc.limit[1] !== 0) {
					__loc.marker.draw(selectedContour!);
				}
			}

			appDbRef.current!.getContours().map((contour) => contour.updateStroke());
			setIsConstraintsModalVisible(false);
		},
		onCancel: () => {
			toolsHandlerRef.current!.activateToolByName('select');
			setIsConstraintsModalVisible(false);
		},
	}

	const onInputApplied = (): void => {
		setIsHoverToolbarMenuVisible(false);
	}

	// get canvas to initialize PaperJS
	useEffect(() => {
		const canvas = canvasRef.current as HTMLCanvasElement;
		canvas.width = canvas.parentElement!.clientWidth;
		canvas.height = canvas.parentElement!.clientHeight;
		Paper.setup(canvas);

		drawingHandlerRef.current = new DrawingHandler(canvas);
		appDbRef.current = new AppDatabase();
		selectionHandlerRef.current = new SelectionHandler();
		toolsHandlerRef.current = new ToolsHandler(appDbRef.current, selectionHandlerRef.current, onInputRequest, onInputApplied);
		fileHandlerRef.current = new FileHandler(appDbRef.current);
	}, [drawingHandlerRef, appDbRef, selectionHandlerRef, toolsHandlerRef, fileHandlerRef]);

	return (
		<div className="App">
			<AppContext.Provider value={{getToolsHandler: () => toolsHandlerRef.current, constraintsFunctions, getFileHandler: () => fileHandlerRef.current }}>
			<AppHeader onToolApply={onToolApply} />
			<div className="Content">
				<canvas id="canvas" onContextMenu={(e) => e.preventDefault()} ref={canvasRef} />
				{ isHoverToolbarMenuVisible &&
					<HoverInputRequestMenu x={hoverToolbarMenuX} onApply={onToolApply} y={hoverToolbarMenuY} />
				}
			</div>
			<ConstraintsModal isOpen={isConstraintsModalVisible} selectionHandler={selectionHandlerRef!.current!} />
			<AppFooter />
			</AppContext.Provider>
		</div>
	);
}

export default App;

